#version 330 core

//Colores recogidos desde shader de vertices
in vec3 color;
//Variable con la entrada de textura
in vec2 texCoord; // Para poder acceder, necesitamos una textura
//Variable para la normal
in vec3 normalVert;

//Textura en 2D
uniform sampler2D colorTex;

out vec4 outColor;



void main(){
	
	outColor = vec4(abs(normalVert), 0.0);
	
}

#version 330 core

in vec3 inPos;		

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;
uniform mat4 modelViewProj;

//Para pasarle el color a los fragmentos
out vec3 color;

void main()
{
	color = ( mod(gl_VertexID,2) == 0) ? vec3(0.0,0.0,1.0):vec3(1.0);
	gl_Position =  modelViewProj * vec4(inPos, 1.0);
}

#version 330 core

in vec3 inPos;
in vec3 inColor;
in vec2 inTexCoord;
in vec3 inNormal;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;
uniform mat4 modelViewProj;
uniform mat4 normal;

//Para pasarle el color a los fragmentos
out vec3 color;
//Se crea una variable para pasar las coordenadas de textura
out vec2 texCoord;

//Se crea para la normal
out vec3 normalVert;

void main()
{
	texCoord = inTexCoord;
	color = inColor;
	normalVert = vec3(normal * vec4(inNormal, 0.0)); //vec4 tiene que tener un 0 porque la normal es un vector no un punto!
	gl_Position =  modelViewProj * vec4(inPos, 1.0);
}

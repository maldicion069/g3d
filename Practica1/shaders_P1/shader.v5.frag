#version 330 core

//Colores recogidos desde shader de vertices
in vec3 color;
out vec4 outColor;

void main(){
	
	outColor = vec4(color,0.0);
}

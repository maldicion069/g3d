#version 330 core

in vec3 inPos;		

void main()
{
	
	mat4 view = mat4(1.0);
	view[3].z = -6.0;
	float n = 0.5;
	float f =  50.0;
	mat4 proj = mat4(0.0);
	proj[0].x = 1.0 / tan (3.14159 / 6.0);
	proj[1].y = proj[0].x;
	proj[2].z = (-n - f)/(f-n);
	proj[2].w = -1.0;
	proj[3].z = (-2.0 * n * f) / (f-n);

	gl_Position =  proj * view * vec4(inPos, 1.0);
}

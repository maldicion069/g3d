#version 330 core

//Colores recogidos desde shader de vertices
in vec3 color;
//Variable con la entrada de textura
in vec2 texCoord; // Para poder acceder, necesitamos una textura

//Textura en 2D
uniform sampler2D colorTex;

out vec4 outColor;

void main(){
	
	outColor = texture(colorTex, texCoord);
	//Depurar si la textura tiene sentido
	//outColor = vec4(texCoord, 0.0, 0.0);

}

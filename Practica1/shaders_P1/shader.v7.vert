#version 330 core

in vec3 inPos;
in vec3 inColor;
in vec2 inTexCoord;

uniform mat4 proj;
uniform mat4 view;
uniform mat4 model;
uniform mat4 modelViewProj;

//Para pasarle el color a los fragmentos
out vec3 color;
//Se crea una variable para pasar las coordenadas de textura
out vec2 texCoord;
void main()
{
	texCoord = inTexCoord;
	color = inColor;
	gl_Position =  modelViewProj * vec4(inPos, 1.0);
}

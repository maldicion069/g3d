#pragma once
class Model_OBJ
{
  public: 
	Model_OBJ();			
    float* Model_OBJ::calculateNormal(float* coord1,float* coord2,float* coord3 );
    int Model_OBJ::Load(char *filename);	// Loads the model
	void Model_OBJ::Draw();					// Draws the model on the screen
	void Model_OBJ::Release();				// Release the model
 
	float* normals;							// Stores the normals
    float* Faces_Triangles;					// Stores the triangles
	float* vertexBuffer;					// Stores the points which make the object
	int TotalConnectedPoints;				// Stores the total number of connected verteces
	int TotalConnectedTriangles;			// Stores the total number of connected triangles
};
 
﻿#include "BOX.h"
#include <IGL/IGlib.h>

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <gl/glut.h>
#include "ModelObj.h"
#include <vector>

int oldTimeSinceStart = 0;
float aspect_ratio;

const float proj0x = tan (3.14159f / 6.0f);

//Camera
glm::mat4 view(1.0f);
glm::mat4 proj(0.0f);

//Idenficadores de los objetos de la escena
int objId =-1;
int objId2 = -1;
int objId3 = -1;
Model_OBJ obj;
int oldX = 0, oldY = 0;
bool is_scrolling = false;

bool mouse_move_active = false;

struct BezierPoint {
	glm::vec2 p0;
	glm::vec2 p1;
	glm::vec2 p2;
	glm::vec2 p3;
};

std::vector<BezierPoint> bezierPoints;
size_t bIdx = 0;

//Declaración de CB
void resizeFunc(int width, int height); //Escalado: nş de pixeles de ancho y de alto
void idleFunc(); //Tiempo ocioso
void keyboardFunc(unsigned char key, int x, int y); // Letra pulsada y la pos del ratón cuando se pulsó
void mouseFunc(int button, int state, int x, int y); // Eventos de ratón 
void mouseMotionFunc(int x, int y);
void animate(int dt);

int main(int argc, char** argv) {
	std::locale::global(std::locale("spanish"));// acentos ;)
	if (!IGlib::init("../shaders_P1/shader.v7.vert", "../shaders_P1/shader.v7.frag"))
		return -1;
   
	//CBs
	IGlib::setResizeCB(resizeFunc);
	IGlib::setIdleCB(idleFunc);
	IGlib::setKeyboardCB(keyboardFunc);
	IGlib::setMouseCB(mouseFunc);
  	IGlib::setMouseMoveCB(mouseMotionFunc);

	//Creamos el objeto que vamos a visualizar
	objId = IGlib::createObj(cubeNTriangleIndex, cubeNVertex, cubeTriangleIndex, 
			cubeVertexPos, cubeVertexColor, cubeVertexNormal,cubeVertexTexCoord, cubeVertexTangent);
	
	objId2 = IGlib::createObj(cubeNTriangleIndex, cubeNVertex, cubeTriangleIndex, 
			cubeVertexPos, cubeVertexColor, cubeVertexNormal,cubeVertexTexCoord, cubeVertexTangent);

	objId3 = IGlib::createObj(cubeNTriangleIndex, cubeNVertex, cubeTriangleIndex, 
			cubeVertexPos, cubeVertexColor, cubeVertexNormal,cubeVertexTexCoord, cubeVertexTangent);

	glm::mat4 modelMat = glm::mat4(1.0f);
	IGlib::setModelMat(objId, modelMat);

	obj.Load("suzanne.obj");

	//Agregado para la textura
	IGlib::addColorTex(objId, "../img/color.png");
	IGlib::addColorTex(objId2, "../img/specMap.png");
	IGlib::addColorTex(objId3, "../img/emissive.png");
	//Incluir texturas aquí.
	
	//CBs
	IGlib::setIdleCB(idleFunc);
	IGlib::setResizeCB(resizeFunc);
	IGlib::setKeyboardCB(keyboardFunc);
	IGlib::setMouseCB(mouseFunc);
	
	BezierPoint bp1;
	float esp = 30.0f;
	bp1.p0 = glm::vec2(20.0f, -5.0f + esp);
	bp1.p1 = glm::vec2(20.0f + esp, -5.0f);
	bp1.p2 = glm::vec2(20.0f, -5.0f - esp);
	bp1.p3 = glm::vec2(20.0f - esp, -5.0f);
	bezierPoints.push_back(bp1);
	
	BezierPoint bp2;
	bp2.p0 = glm::vec2(20.0f, -5.0f - esp);
	bp2.p1 = glm::vec2(20.0f - esp, -5.0f);
	bp2.p2 = glm::vec2(20.0f, -5.0f + esp);
	bp2.p3 = glm::vec2(20.0f + esp, -5.0f);
	bezierPoints.push_back(bp2);
	bIdx = 0;
	
	//Mainloop
	IGlib::mainLoop();
	IGlib::destroy();
	return 0;
}

void resizeFunc(int width, int height)
{
	//Ajusta el aspect ratio al tamańo de la venta
	//Tener en cuenta la división por 0...
	//Se ajusta la cámara
	//Si no se da valor se cogen valores por defecto
	aspect_ratio = (float)height/(float)width;
	std::cout << aspect_ratio << std::endl;
	view[3].z = -20.0f;
	float n = 0.5f;
	float f =  500.0f;

	proj[0].x =  1.0f / ( proj0x * aspect_ratio);
	proj[1].y = proj[0].x / aspect_ratio;
	proj[2].z = (-n - f)/(f-n);
	proj[2].w = -1.0f;
	proj[3].z = (-2.0f * n * f) / (f-n);

	IGlib::setProjMat(proj);
	IGlib::setViewMat(view);
}

void animate(int dt) {
	static float angle = 0.0f;
	static float angle_orbital = 0.1f;

	angle = (angle > 2.0f*3.14159f) ? 0.0f:angle+0.005f*dt;
	glm::mat4 model(1.0f);
	model = glm::rotate(model, angle, glm::vec3(1.0f,1.0f,0.0f));
	IGlib::setModelMat(objId, model);

	angle_orbital = (angle_orbital > 2.0f*3.14159f) ? 0.0f:angle_orbital+0.005f*dt;
	glm::mat4 model2(1.0f);
	model2 = glm::rotate(model2, angle_orbital, glm::vec3(0.0f,1.0f,0.0f));
	model2 = glm::translate(model2, glm::vec3(10.0f,0.0f,0.0f));
	IGlib::setModelMat(objId2, model2);

	//BEZIER
	static float t = 0.0f, x, z;
	if (t > 1) {
		t = 0;
		bIdx++;
		bIdx = bIdx >= bezierPoints.size()? 0 : bIdx;
	}
	x = pow(1-t, 3)*bezierPoints[bIdx].p0.x + 3*pow(1-t, 2)*bezierPoints[bIdx].p1.x + 3*pow(1-t, 3)*bezierPoints[bIdx].p2.x + pow(t, 3)*bezierPoints[bIdx].p3.x;
	z = pow(1-t, 3)*bezierPoints[bIdx].p0.y + 3*pow(1-t, 2)*bezierPoints[bIdx].p1.y + 3*pow(1-t, 3)*bezierPoints[bIdx].p2.y + pow(t, 3)*bezierPoints[bIdx].p3.y;
	t += 0.0001f*dt;

	glm::mat4 model3(1.0f);
	model3 = glm::translate(model3, glm::vec3(x, z, 0.0f));
	IGlib::setModelMat(objId3, model3);

	// Draw the object
	obj.Draw();
}

void idleFunc() {
	glClearColor(1, 1, 1, 1);
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
    int deltaTime = currentTime - oldTimeSinceStart;
    oldTimeSinceStart = currentTime;

	animate(deltaTime);
}

void keyboardFunc(unsigned char key, int x, int y) {
	const float MOVEMENT_SPEED = 1.0f;
	const float ANGLE_ROTATION = 0.01f;
	std::cout << "Se ha pulsado la tecla " << key << std::endl << std::endl;

	switch(key){
		//Right movement
	case 'd':
		view = glm::translate(view, glm::vec3(1.0 * -MOVEMENT_SPEED, 0.0f, 0.0f));
		break;
		//Left movement
	case 'a':
		view = glm::translate(view, glm::vec3(1.0 * MOVEMENT_SPEED, 0.0f, 0.0f));
		break;
		//Up movement
	case 'w':
		view = glm::translate(view, glm::vec3(0.0f, 1.0f * -MOVEMENT_SPEED, 0.0f));
		break;
		//Bottom movement
	case 's': 
		view = glm::translate(view, glm::vec3(0.0f, 1.0f * MOVEMENT_SPEED, 0.0f));
		break;
		//Zoom in
	case '+':
		view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f * MOVEMENT_SPEED));
		break;
		//Zoom out
	case '-':
		view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f * -MOVEMENT_SPEED));
		break;
		//Rotate left
	case 'f':
		view = glm::rotate(view, ANGLE_ROTATION, glm::vec3(0.0f, 1.0f, 0.0f));
		break;
		//Rotate right
	case 'h':
		view = glm::rotate(view, -ANGLE_ROTATION, glm::vec3(0.0f, 1.0f, 0.0f));
		break;
	case 't':
		view = glm::rotate(view, ANGLE_ROTATION, glm::vec3(0.0f, 0.0f, 1.0f));
		break;
	case 'g':
		view = glm::rotate(view, -ANGLE_ROTATION, glm::vec3(0.0f, 0.0f, 1.0f));
		break;
	}

	IGlib::setViewMat(view);

}

void mouseFunc(int button, int state, int x, int y) {
	if (state==0) {
		mouse_move_active = true;
		if (button == 1)
			is_scrolling = true;
		oldX = x;
		oldY = y;
		std::cout << "Se ha pulsado el botón ";
	}
	else {
		mouse_move_active = false;
		if (button == 1)
			is_scrolling = false;
		std::cout << "Se ha soltado el botón ";
	}
	
	if (button == 0) std::cout << "de la izquierda del ratón " << std::endl;
	if (button == 1) std::cout << "central del ratón " << std::endl;
	if (button == 2) std::cout << "de la derecha del ratón " << std::endl;

	std::cout << "en la posición " << x << " " << y << std::endl << std::endl;
}

void mouseMotionFunc(int x, int y) {
	const float MOVEMENT_SPEED = 1.0f;
	if(mouse_move_active) {
		if(is_scrolling) {
			float moveY = y - oldY;
			moveY *= 0.1f;
			std::cout << "Nos movemos " << moveY << std::endl;
			view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f*moveY));
			IGlib::setViewMat(view);
			oldY = y;
		} else {
			float moveX = x - oldX;
			moveX *= 0.01f;
			float moveY = y - oldY;
			moveY *= -0.01f;
			std::cout << "Nos movemos " << moveX << "-" << moveY << std::endl;
			view = glm::rotate(view, moveX, glm::vec3(1.0f, 0.0f, 0.0f));
			view = glm::rotate(view, moveY, glm::vec3(0.0f, 1.0f, 0.0f));
			IGlib::setViewMat(view);
			oldX = x;
			oldY = y;
		}
	}
}
#include <iostream>
#include "GBuffer.h"
#include "SimpleGLShader.h"

#include <gl/glew.h>
#include <gl/gl.h>
#include <gl/freeglut.h>

#include "BOX.h"

#define SCREEN_SIZE 500,500

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

SimpleGLShader dfShader;
GBuffer gBuffer;

glm::mat4	proj = glm::mat4(1.0f);
glm::mat4	view = glm::mat4(1.0f);
glm::mat4	model = glm::mat4(1.0f);

int oldTimeSinceStart = 0;

void renderFunc();

void resizeFunc(int width, int height) {

}

void idleFunc() {	
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
    int dt = currentTime - oldTimeSinceStart;
    oldTimeSinceStart = currentTime;

	static float angle = 0.0f;
	static float angle_orbital = 0.1f;

	model = glm::mat4(1.0f);
	angle = (angle > 3.141592f * 2.0f) ? 0 : angle + 0.001f*dt;
	model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 0.0f));
	glutPostRedisplay();
}

void keyboardFunc(unsigned char key, int x, int y) {

}

void mouseFunc(int button, int state, int x, int y) {

}

//VAO
unsigned int vao;
//VBOs que forman parte del objeto
unsigned int posVBO;
unsigned int colorVBO;
unsigned int normalVBO;
unsigned int texCoordVBO;
unsigned int triangleIndexVBO;

int main(int argc, char** argv) {	
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	//glutInitContextProfile(GLUT_COMPATIBILITY_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(SCREEN_SIZE);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Prácticas GLSL");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit(-1);
	}

	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;

	if (!gBuffer.Init(500, 500)) {
        return -1;
    }

	dfShader.load("../shaders_P4/geometry_pass.vert", GL_VERTEX_SHADER);
	dfShader.load("../shaders_P4/geometry_pass.frag", GL_FRAGMENT_SHADER);

	dfShader.create();
	dfShader.link();

	dfShader.bind_attribute("inPos", 0);
	dfShader.bind_attribute("inTexCoord", 1);
	dfShader.bind_attribute("inNormal", 2);

	dfShader.add_uniform("modelViewProj");
	dfShader.add_uniform("modelView");
	dfShader.add_uniform("normal");
	
	
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	//Habilitamos el culling
	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	proj = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 50.0f);
	view = glm::mat4(1.0f);
	view[3].z = -6;


	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	if (dfShader.attribute("inPos") != -1){
		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(dfShader.attribute("inPos"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(dfShader.attribute("inPos"));
	}

	if (dfShader.attribute("inColor") != -1){
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO); //GL_ARRAY_BUFFERS para atributos
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexColor, GL_STATIC_DRAW);
		glVertexAttribPointer(dfShader.attribute("inColor"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(dfShader.attribute("inColor"));
	}

	if (dfShader.attribute("inNormal") != -1){
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexNormal, GL_STATIC_DRAW);
		glVertexAttribPointer(dfShader.attribute("inNormal"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(dfShader.attribute("inNormal"));
	}

	if (dfShader.attribute("inTexCoord") != -1){
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 2, cubeVertexTexCoord, GL_STATIC_DRAW);
		glVertexAttribPointer(dfShader.attribute("inTexCoord"), 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(dfShader.attribute("inTexCoord"));
	}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, cubeNTriangleIndex*sizeof(unsigned int) * 3, cubeTriangleIndex, GL_STATIC_DRAW);
	
	model = glm::mat4(1.0f);









	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);

	std::cout << "HOLA";

	glutMainLoop();

	return 0;
}

void DSLights() {
    glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

    gBuffer.BindForReading();
    glClear(GL_COLOR_BUFFER_BIT);
}

void DSLightPass() {
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	gBuffer.BindForReading();

	int WINDOW_WIDTH = 500;
	int WINDOW_HEIGHT = 500;
	GLint HalfWidth = (GLint)(WINDOW_WIDTH / 2.0f);
    GLint HalfHeight = (GLint)(WINDOW_HEIGHT / 2.0f);
        
    gBuffer.SetReadBuffer(GBuffer::GBUFFER_TEXTURE_TYPE_POSITION);
    glBlitFramebuffer(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, 0, HalfWidth, HalfHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);

    gBuffer.SetReadBuffer(GBuffer::GBUFFER_TEXTURE_TYPE_DIFFUSE);
    glBlitFramebuffer(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, 0, HalfHeight, HalfWidth, WINDOW_HEIGHT, GL_COLOR_BUFFER_BIT, GL_LINEAR);

    gBuffer.SetReadBuffer(GBuffer::GBUFFER_TEXTURE_TYPE_NORMAL);
    glBlitFramebuffer(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, HalfWidth, HalfHeight, WINDOW_WIDTH, WINDOW_HEIGHT, GL_COLOR_BUFFER_BIT, GL_LINEAR);

    gBuffer.SetReadBuffer(GBuffer::GBUFFER_TEXTURE_TYPE_TEXCOORD);
    glBlitFramebuffer(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, HalfWidth, 0, WINDOW_WIDTH, HalfHeight, GL_COLOR_BUFFER_BIT, GL_LINEAR);	
}

void DSGeometryPass() {
	dfShader.use();
	gBuffer.BindForWriting();
    // Only the geometry pass updates the depth buffer
    glDepthMask(GL_TRUE);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);

    glDisable(GL_BLEND);

	// Enviamos matriz perspectiva, normal y modelView.
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));
	if (dfShader.uniform("modelView") != -1) {
		glUniformMatrix4fv(dfShader.uniform("modelView"), 1, GL_FALSE,&(modelView[0][0]));
	}
	if (dfShader.uniform("modelViewProj") != -1) {
		glUniformMatrix4fv(dfShader.uniform("modelViewProj"), 1, GL_FALSE, &(modelViewProj[0][0]));
	}
	if (dfShader.uniform("normal") != -1) {
		glUniformMatrix4fv(dfShader.uniform("normal"), 1, GL_FALSE,&(normal[0][0]));
	}
	
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex*3,GL_UNSIGNED_INT, (void*)0);

	dfShader.unuse();

	glDepthMask(GL_FALSE);
        
    glDisable(GL_DEPTH_TEST);
}
void renderFunc() {
	gBuffer.StartFrame();
	DSGeometryPass();
	
	// We need stencil to be enabled in the stencil pass to get the stencil buffer
    // updated and we also need it in the light pass because we render the light
    // only if the stencil passes.
    glEnable(GL_STENCIL_TEST);
    glDisable(GL_STENCIL_TEST);

	DSLights();
	DSLightPass();
	glutSwapBuffers();
}

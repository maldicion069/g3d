#version 120
in vec3 position;
in vec3	normal;
in vec2 texCoord;

uniform sampler2D colorTex;

//layout(location = 0) out vec4 outColor;
//layout(location = 1) out vec4 outPosition;
//layout(location = 2) out vec4 outNormal;

void main() {
	gl_FragData[0]	= vec4(texture2D(colorTex, texCoord).rgb, 0);	// 3.3: outColor = vec4(texture2D(colorTex, texCoord).rgb, 0);
	gl_FragData[1]	= vec4(position, 0);							// 3.3: outPosition = vec4(position, 0);
	gl_FragData[2]	= vec4(normal, 0);								// 3.3: outNormal = vec4(normal, 0);
}

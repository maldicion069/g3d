#version 120

struct GBuffer {
	sampler2D diffuse;
	sampler2D position;
	sampler2D normals;
};
uniform GBuffer gBuffer;

uniform vec3 lightPos;

vec3 cameraPosition = vec3(0, 0, 0);

in vec2 texCoord;

//layout(location = 0) out vec4 outColor;

void main() {
	// En 3.3 se usa texture
	vec4 diffuse	=	texture2D( gBuffer.diffuse, texCoord );				// 3.3: texture( gBuffer.diffuse, texCoord);
	vec3 position	=	texture2D( gBuffer.position, texCoord ).xyz;		// 3.3: texture( gBuffer.position, texCoord);
	vec4 normal		=	texture2D( gBuffer.normals, texCoord );				// 3.3: texture( gBuffer.normals, texCoord);
	
	vec3 lightDir = lightPos - position;
	
	normal = normalize(normal);
	lightDir = normalize(lightDir);
	
	vec3 eyeDir = normalize(cameraPosition-position);
	vec3 vHalfVector = normalize(lightDir+eyeDir);
	
	// C�digo legado: Deber�a ser una variable out con layout = 0
	gl_FragColor = max(dot(normal,vec4(lightDir, 1.0)),0) * diffuse + 		// 3.3: outColor = ...
					pow(max(dot(normal,vec4(vHalfVector, 1.0)),0.0), 100) * 1.5;
}

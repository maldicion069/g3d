#version 120
uniform mat4 WorldMatrix;

/*in vec3 inPos;	
in vec3 inColor;
in vec3 inNormal;

uniform mat4 modelViewProj;
uniform mat4 modelView;
uniform mat4 normal;*/

varying vec2 texCoord;		// 3.3: En versiones modernas de OpenGL se usa out
varying vec3 position;		// 3.3: En versiones modernas de OpenGL se usa out
varying vec3 normal;		// 3.3: En versiones modernas de OpenGL se usa out

void main() {
	// Move the normals back from the camera space to the world space
	mat3 worldRotationInverse = transpose(mat3(WorldMatrix));							// 3.3: Env�o esta matriz desde el cliente
	normal			= normalize(worldRotationInverse * gl_NormalMatrix * gl_Normal);	// 3.3: (normal * vec4(inNormal, 0.0)).xyz
	position		= (gl_ModelViewMatrix * gl_Vertex).xyz;								// 3.3: (modelView * vec4(inPos, 1.0)).xyz
	gl_Position		= gl_ModelViewProjectionMatrix * gl_Vertex;							// 3.3: modelViewProj * vec4 (inPos,1.0);
	texCoord = gl_MultiTexCoord0.xy;													// 3.3: texCoord
}

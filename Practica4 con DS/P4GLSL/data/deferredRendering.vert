#version 120

varying vec2 texCoord;		// 3.3: En versiones modernas de OpenGL se usa out
//uniform mat4 viewProj;

// uniform mat4 modelViewProj;
// in vec3 inPos;
// int vec2 texCoord;

void main() {
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;	// 3.3: modelViewProj * vec4 (inPos,1.0);
	texCoord = gl_MultiTexCoord0.xy;						// 3.3: texCoord;

    gl_FrontColor = vec4(1.0, 1.0, 1.0, 1.0);
}

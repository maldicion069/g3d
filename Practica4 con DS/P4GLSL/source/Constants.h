#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#ifndef GLOBALVAR_H
#define GLOBALVAR_H

	const float D2R = 3.1415/180.f;

	namespace Constants {
		extern glm::mat4 proj;
		extern glm::mat4 view;
	};

#endif
#pragma once
#include "Model.h"

class PlaneModel : public Model {
public:
	// Methods
	PlaneModel(const std::string& vsFile, const std::string& fsFile, float side);

	void render();

protected:
	// Fields
	float m_side;
};


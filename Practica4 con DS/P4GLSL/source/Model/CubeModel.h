#pragma once
#include "Model.h"

class CubeModel : public Model {
public:
	// Methods
	CubeModel(const std::string& vsFile, const std::string& fsFile, float side);
	virtual ~CubeModel();

	void render();

protected:
	// Fields
	float m_side;

	/*unsigned int vao;

	unsigned int posVBO;
	unsigned int colorVBO;
	unsigned int normalVBO;
	unsigned int texCoordVBO;
	unsigned int triangleIndexVBO;*/
};


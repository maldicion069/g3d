#include "TubeModel.h"
#include <math.h>

TubeModel::TubeModel(const std::string& vsFile, const std::string& fsFile, float baseRadius, float topRadius, float height, unsigned int slices, unsigned int meshPrecision)
	: Model(vsFile, fsFile) {	
	m_baseRadius = baseRadius;
	m_topRadius = topRadius;
	m_height = height;
	m_slices = slices;
	m_meshPrecision = meshPrecision;
}

void TubeModel::render() {
	glPushMatrix();

	// Save the current world matrix to compensate the normals in the shader
	float worldMatrix[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, worldMatrix);

	m_model = glm::mat4(1.0f);
	m_model = glm::translate(m_model, m_position);
	m_model = glm::rotate(m_model, D2R*m_rotation.x, glm::vec3(1, 0, 0));
	m_model = glm::rotate(m_model, D2R*m_rotation.y, glm::vec3(0, 1, 0));
	m_model = glm::rotate(m_model, D2R*m_rotation.z, glm::vec3(0, 0, 1));
	glMultMatrixf(glm::value_ptr(m_model));
	
	m_shader.use();

	glBindTexture(GL_TEXTURE_2D, m_texture);
	glUniform1i (m_shader.uniform(m_textureID), 0);
	glUniformMatrix4fv (m_shader.uniform(m_worldMatrixID), 1, false, worldMatrix);
	
	// 3.3: Esto deber�a mandarse usando vao y vbo
 	GLUquadricObj *tube = gluNewQuadric();
	gluQuadricTexture(tube, true);
	gluCylinder(tube, m_baseRadius, m_topRadius, m_height, m_slices, m_meshPrecision);
	gluDeleteQuadric(tube);
	
	m_shader.unuse();
	
	glPopMatrix();
}


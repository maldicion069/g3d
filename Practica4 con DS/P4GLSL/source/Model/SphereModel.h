#pragma once
#include "Model.h"

class SphereModel : public Model {
public:
	// Methods
	SphereModel(const std::string& vsFile, const std::string& fsFile, float radius, unsigned int meshPrecision);

	void render();

private:
	// Fields
	float			m_radius;
	unsigned int	m_meshPrecision;
};


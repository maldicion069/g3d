#include "CubeModel.h"
#include <math.h>
#include "../Constants.h"
//#include "BOX.h"

CubeModel::CubeModel(const std::string& vsFile, const std::string& fsFile, float side)
	: Model(vsFile, fsFile), m_side(side) {	
	
	/*glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	m_shader.add_attribute("inPos");
	m_shader.add_attribute("inNormal");
	if (m_shader.attribute("inPos") != -1){
		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(m_shader.attribute("inPos"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(m_shader.attribute("inPos"));
	}
	
	if (m_shader.attribute("inNormal") != -1){
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexNormal, GL_STATIC_DRAW);
		glVertexAttribPointer(m_shader.attribute("inNormal"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(m_shader.attribute("inNormal"));
	}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, cubeNTriangleIndex*sizeof(unsigned int) * 3, cubeTriangleIndex, GL_STATIC_DRAW);*/
}

CubeModel::~CubeModel() {
	/*glBindVertexArray(0);
	glDeleteVertexArrays(1, &vao);*/
}
#include <iostream>
void CubeModel::render() {

	/*glm::mat4 modelView = Constants::view * m_model;
	glm::mat4 modelViewProj = Constants::proj * Constants::view * m_model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));
	
	m_shader.add_uniform("modelView");
	m_shader.add_uniform("modelViewProj");
	m_shader.add_uniform("normal");

	if(m_shader.uniform("modelView") != -1) {
		glUniformMatrix4fv(m_shader.uniform("modelView"), 1, GL_FALSE, &(modelView[0][0]));
	}
	if (m_shader.uniform("modelViewProj") != -1) {
		glUniformMatrix4fv(m_shader.uniform("modelViewProj"), 1, GL_FALSE, &(modelViewProj[0][0]));
	}
	if (m_shader.uniform("normal") != -1) {
		glUniformMatrix4fv(m_shader.uniform("normal"), 1, GL_FALSE,&(normal[0][0]));
	}

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex * 3, GL_UNSIGNED_INT, (void*)0);*/

	glPushMatrix();

	// Save the current world matrix to compensate the normals in the shader
	float worldMatrix[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, worldMatrix);

	m_model = glm::mat4(1.0f);
	m_model = glm::scale(m_model, glm::vec3(m_side, m_side, m_side));
	m_model = glm::translate(m_model, m_position);
	m_model = glm::rotate(m_model, D2R*m_rotation.x, glm::vec3(1, 0, 0));
	m_model = glm::rotate(m_model, D2R*m_rotation.y, glm::vec3(0, 1, 0));
	m_model = glm::rotate(m_model, D2R*m_rotation.z, glm::vec3(0, 0, 1));
	glMultMatrixf(glm::value_ptr(m_model));

	m_shader.use();

	glBindTexture(GL_TEXTURE_2D, m_texture);
	glUniform1i( m_shader.uniform(m_textureID), 0);
	glUniformMatrix4fv( m_shader.uniform(m_worldMatrixID), 1, false, worldMatrix);

	// 3.3: Esto deber�a mandarse usando vao y vbo
	glBegin(GL_QUADS);
		// Front Face
		glTexCoord2f(0.0f, 0.0f); glNormal3f(0.0f, 0.0f,  1.0f); glVertex3f(-1.0f, -1.0f,  1.0f); 
		glTexCoord2f(1.0f, 0.0f); glNormal3f(0.0f, 0.0f,  1.0f); glVertex3f( 1.0f, -1.0f,  1.0f); 
		glTexCoord2f(1.0f, 1.0f); glNormal3f(0.0f, 0.0f,  1.0f); glVertex3f( 1.0f,  1.0f,  1.0f); 
		glTexCoord2f(0.0f, 1.0f); glNormal3f(0.0f, 0.0f,  1.0f); glVertex3f(-1.0f,  1.0f,  1.0f); 
		// Back Face
		glTexCoord2f(1.0f, 0.0f); glNormal3f(0.0f, 0.0f, -1.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
		glTexCoord2f(1.0f, 1.0f); glNormal3f(0.0f, 0.0f, -1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
		glTexCoord2f(0.0f, 1.0f); glNormal3f(0.0f, 0.0f, -1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
		glTexCoord2f(0.0f, 0.0f); glNormal3f(0.0f, 0.0f, -1.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
		// Top Face
		glTexCoord2f(0.0f, 1.0f); glNormal3f(0.0f, 1.0f,  0.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
		glTexCoord2f(0.0f, 0.0f); glNormal3f(0.0f, 1.0f,  0.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
		glTexCoord2f(1.0f, 0.0f); glNormal3f(0.0f, 1.0f,  0.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
		glTexCoord2f(1.0f, 1.0f); glNormal3f(0.0f, 1.0f,  0.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
		// Bottom Face
		glTexCoord2f(1.0f, 1.0f); glNormal3f(0.0f, -1.0f,  0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
		glTexCoord2f(0.0f, 1.0f); glNormal3f(0.0f, -1.0f,  0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
		glTexCoord2f(0.0f, 0.0f); glNormal3f(0.0f, -1.0f,  0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
		glTexCoord2f(1.0f, 0.0f); glNormal3f(0.0f, -1.0f,  0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
		// Right face
		glTexCoord2f(1.0f, 0.0f); glNormal3f(1.0f, 0.0f,  0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
		glTexCoord2f(1.0f, 1.0f); glNormal3f(1.0f, 0.0f,  0.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
		glTexCoord2f(0.0f, 1.0f); glNormal3f(1.0f, 0.0f,  0.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
		glTexCoord2f(0.0f, 0.0f); glNormal3f(1.0f, 0.0f,  0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
		// Left Face
		glTexCoord2f(0.0f, 0.0f); glNormal3f(-1.0f, 0.0f,  0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
		glTexCoord2f(1.0f, 0.0f); glNormal3f(-1.0f, 0.0f,  0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
		glTexCoord2f(1.0f, 1.0f); glNormal3f(-1.0f, 0.0f,  0.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
		glTexCoord2f(0.0f, 1.0f); glNormal3f(-1.0f, 0.0f,  0.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
		glEnd();
		
	m_shader.unuse();
	glPopMatrix();
}
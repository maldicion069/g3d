#include "PlaneModel.h"
#include <math.h>

PlaneModel::PlaneModel(const std::string& vsFile, const std::string& fsFile, float side)
	: Model(vsFile, fsFile)
	, m_side(side) {}

void PlaneModel::render() {
	glPushMatrix();

	// Save the current world matrix to compensate the normals in the shader
	float worldMatrix[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, worldMatrix);
	
	m_model = glm::mat4(1.0f);
	m_model = glm::translate(m_model, m_position);
	m_model = glm::rotate(m_model, D2R*m_rotation.x, glm::vec3(1, 0, 0));
	m_model = glm::rotate(m_model, D2R*m_rotation.y, glm::vec3(0, 1, 0));
	m_model = glm::rotate(m_model, D2R*m_rotation.z, glm::vec3(0, 0, 1));
	m_model = glm::scale(m_model, glm::vec3(m_side, m_side, m_side));
	glMultMatrixf(glm::value_ptr(m_model));

	m_shader.use();

	glBindTexture(GL_TEXTURE_2D, m_texture);
	glUniform1i ( m_shader.uniform(m_textureID), 0);
	glUniformMatrix4fv ( m_shader.uniform(m_worldMatrixID), 1, false, worldMatrix);
	
	// 3.3: Esto deber�a mandarse usando vao y vbo
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 1.0f); 
		glNormal3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f,  0.0f, -1.0f);

		glTexCoord2f(0.0f, 0.0f); 
		glNormal3f(0.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f,  0.0f,  1.0f);

		glTexCoord2f(1.0f, 0.0f); 
		glNormal3f(0.0f, 1.0f, 0.0f);
		glVertex3f( 1.0f,  0.0f,  1.0f);

		glTexCoord2f(1.0f, 1.0f); 
		glNormal3f(0.0f, 1.0f, 0.0f);
		glVertex3f( 1.0f,  0.0f, -1.0f);
	glEnd();
	
	m_shader.unuse();

	glPopMatrix();
}


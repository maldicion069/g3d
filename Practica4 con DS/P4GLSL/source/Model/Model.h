#pragma once
#include <windows.h>
#include "gl/glew.h"
#include <gl/gl.h>
#include <gl/glu.h>
#include <string>
#include "../SimpleGLShader.h"
#include "../Constants.h"

class Model {
public:
	Model(const std::string& sVSFileName, const std::string& sFSFileName);
	virtual ~Model() {}

	bool			loadTexture(const std::string& textureName);
	void			setPosition(float x, float y, float z);
	void			setRotation(float x, float y, float z);
	void			addRotation(float x, float y, float z);
	virtual void	render() = 0;

protected:
	SimpleGLShader 	m_shader; // Every model must have a shader associated (both vertex and fragment)

	char*			m_worldMatrixID; // This ID is used to pass the world matrix into the shader
	glm::vec3		m_rotation;
	glm::vec3		m_position;
	
	char*			m_textureID; // Texture ID used to pass the texture into the shader
	GLuint			m_texture; // OpenGL texture ID
	
	glm::mat4		m_model;
};


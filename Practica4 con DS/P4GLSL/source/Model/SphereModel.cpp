#include "SphereModel.h"
#include <math.h>

SphereModel::SphereModel(const std::string& vsFile, const std::string& fsFile, float radius, unsigned int meshPrecision)
	: Model(vsFile, fsFile) {	
	m_radius = radius;
	m_meshPrecision = meshPrecision;
}

void SphereModel::render() {
	glPushMatrix();

	// Save the current world matrix to compensate the normals in the shader
	float worldMatrix[16];
	glGetFloatv(GL_MODELVIEW_MATRIX, worldMatrix);

	m_model = glm::mat4(1.0f);
	m_model = glm::translate(m_model, m_position);
	m_model = glm::rotate(m_model, D2R*m_rotation.x, glm::vec3(1, 0, 0));
	m_model = glm::rotate(m_model, D2R*m_rotation.y, glm::vec3(0, 1, 0));
	m_model = glm::rotate(m_model, D2R*m_rotation.z, glm::vec3(0, 0, 1));
	glMultMatrixf(glm::value_ptr(m_model));
	
	m_shader.use();

	glBindTexture(GL_TEXTURE_2D, m_texture);
	glUniform1i (m_shader.uniform(m_textureID), 0);
	glUniformMatrix4fv (m_shader.uniform(m_worldMatrixID), 1, false, worldMatrix);
	
	// 3.3: Esto deber�a mandarse usando vao y vbo
 	GLUquadricObj *sphere = gluNewQuadric();
	gluQuadricTexture(sphere, true);
	gluSphere(sphere, m_radius, m_meshPrecision, m_meshPrecision);
	gluDeleteQuadric(sphere);
	
	m_shader.unuse();
	
	glPopMatrix();
}


#pragma once
#include "Model.h"

class TubeModel : public Model {
public:
	// Methods
	TubeModel(const std::string& vsFile, const std::string& fsFile, float baseRadius, float topRadius, float height, unsigned int slices, unsigned int meshPrecision);

	void render();

private:
	// Fields
	float			m_baseRadius;
	float			m_topRadius;
	float			m_height;
	unsigned int	m_slices;
	unsigned int	m_meshPrecision;
};
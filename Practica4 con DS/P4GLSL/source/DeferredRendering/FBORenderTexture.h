#pragma once
#include <windows.h>
#include "GL/glew.h"
#include "GL/gl.h"
#include "GL/freeglut.h"
#include "../Constants.h"

class FBORenderTexture {
public:
	// Ctors/Dtors
	FBORenderTexture(int w, int h);
	~FBORenderTexture();

	// Methods
	void start();
	void stop();
	void showTexture(unsigned int i, float fSizeX = 400, float fSizeY = 400, float x = 0, float y = 0) const;

	GLuint diffuseTexture() const { return m_diffuseTexture; }
	GLuint positionTexture() const { return m_positionTexture; }
	GLuint normalsTexture() const { return m_normalsTexture; }

	void resize(int w, int h) { 
		// TODO
	}
private:
	GLuint			m_fbo; // The FBO ID
	GLuint			m_diffuseRT; // The diffuse render target
	unsigned int	m_diffuseTexture; // The OpenGL texture for the diffuse render target
	GLuint			m_positionRT; // The position render target
	unsigned int	m_positionTexture; // The OpenGL texture for the position render target
	GLuint			m_normalsRT; // The normals render target
	unsigned int	m_normalsTexture; // The OpenGL texture for the normals render target
	GLuint			m_depthBuffer; // Depth buffer handle

	unsigned int	m_width; // FBO width
	unsigned int	m_height; // FBO height
};

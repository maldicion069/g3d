#include "FBORenderTexture.h"
#include "DeferredRendering.h"
#include <exception>
#include "../Application.h"

DeferredRendering::DeferredRendering(int w, int h, FBORenderTexture* m_fboRT)
	: m_shader("data/deferredRendering.vert", "data/deferredRendering.frag"), m_fboRT(m_fboRT), m_width(w), m_height(h) {	
	
	// Get Uniforms
	m_shader.add_uniform(m_diffuseID = "gBuffer.diffuse");
	m_shader.add_uniform(m_positionID = "gBuffer.position");
	m_shader.add_uniform(m_normalsID = "gBuffer.normals");
	m_shader.add_uniform(m_lightPosition = "lightPos");
	m_shader.add_uniform(m_viewProj = "viewProj");
}

void DeferredRendering::render() {
	//Projection setup
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0,m_width,0,m_height,0.1f,2);	
	
	//Model setup
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	
	m_shader.use();

	/*GLfloat mp[16];
	glGetFloatv(GL_PROJECTION, mp);
	glUniformMatrix4fv(m_shader.uniform(m_viewProj), sizeof(mp), GL_FALSE, &mp[0]);*/

	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_fboRT->diffuseTexture());
	glUniform1i(m_shader.uniform(m_diffuseID), 0);
	
	glActiveTexture(GL_TEXTURE1);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_fboRT->positionTexture());
	glUniform1i(m_shader.uniform(m_positionID), 1);
	
	glActiveTexture(GL_TEXTURE2);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, m_fboRT->normalsTexture());
	glUniform1i(m_shader.uniform(m_normalsID), 2);

	// Send light position
	glUniform3fv(m_shader.uniform(m_lightPosition), 1, glm::value_ptr(Application::getInstance()->lightPosition()));

	// Render the quad
	glLoadIdentity();
	glColor3f(1,1,1);
	glTranslatef(0,0,-1.0);
	
	glBegin(GL_QUADS);
		glTexCoord2f( 0, 0 );
		glVertex3f(0.0f, 0.0f, 0.0f);
		glTexCoord2f( 1, 0 );
		glVertex3f((float) m_width, 0.0f, 0.0f);
		glTexCoord2f( 1, 1 );
		glVertex3f((float) m_width, (float) m_height, 0.0f);
		glTexCoord2f( 0, 1 );
		glVertex3f(0.0f,  (float) m_height, 0.0f);
	glEnd();
	
	// Reset OpenGL state
	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE1);
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	glActiveTexture(GL_TEXTURE2);
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	m_shader.unuse();
	
	//Reset to the matrices	
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
	
}
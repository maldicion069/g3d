#include "FBORenderTexture.h"
#include <exception>

FBORenderTexture::FBORenderTexture(int w, int h) {	
	m_width  = w;
	m_height = h;

	// Generate the OGL resources for what we need
	glGenFramebuffers(1, &m_fbo);
	glGenRenderbuffers(1, &m_diffuseRT);
	glGenRenderbuffers(1, &m_positionRT);
	glGenRenderbuffers(1, &m_normalsRT);
	glGenRenderbuffers(1, &m_depthBuffer);

	// Bind the FBO so that the next operations will be bound to it
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);

	// Bind the diffuse render target
	glBindRenderbuffer(GL_RENDERBUFFER, m_diffuseRT);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA, m_width, m_height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, m_diffuseRT);

	// Bind the position render target
	glBindRenderbuffer(GL_RENDERBUFFER, m_positionRT);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA32F, m_width, m_height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_RENDERBUFFER, m_positionRT);

	// Bind the normal render target
	glBindRenderbuffer(GL_RENDERBUFFER, m_normalsRT);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA16F, m_width, m_height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_RENDERBUFFER, m_normalsRT);

	// Bind the depth buffer
	glBindRenderbuffer(GL_RENDERBUFFER, m_depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, m_width, m_height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_depthBuffer);

	// Generate and bind the OGL texture for diffuse
	glGenTextures(1, &m_diffuseTexture);
	glBindTexture(GL_TEXTURE_2D, m_diffuseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	// Attach the texture to the FBO
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_diffuseTexture, 0);

	// Generate and bind the OGL texture for positions
	glGenTextures(1, &m_positionTexture);
	glBindTexture(GL_TEXTURE_2D, m_positionTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	// Attach the texture to the FBO
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, m_positionTexture, 0);

	// Generate and bind the OGL texture for normals
	glGenTextures(1, &m_normalsTexture);
	glBindTexture(GL_TEXTURE_2D, m_normalsTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, m_width, m_height, 0, GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	// Attach the texture to the FBO
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, m_normalsTexture, 0);

	// Check if all worked fine and unbind the FBO
	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER)) {
		throw new std::exception("Can't initialize an FBO render texture. FBO initialization failed.");
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

FBORenderTexture::~FBORenderTexture() {
	// Delete textures
	glDeleteTextures(1, &m_normalsTexture);
	glDeleteTextures(1, &m_positionTexture);
	glDeleteTextures(1, &m_diffuseTexture);

	glDeleteFramebuffers(1, &m_fbo);

	glDeleteRenderbuffers(1, &m_diffuseRT);
	glDeleteRenderbuffers(1, &m_positionRT);
	glDeleteRenderbuffers(1, &m_normalsRT);
	glDeleteRenderbuffers(1, &m_depthBuffer);
}

void FBORenderTexture::start() {
	// Bind our FBO and set the viewport to the proper size
	glBindFramebuffer(GL_FRAMEBUFFER, m_fbo);
	glPushAttrib(GL_VIEWPORT_BIT);
	glViewport(0,0,m_width, m_height);

	// Clear the render targets
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );

	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);

	// Specify what to render an start acquiring
	GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, buffers);
}

void FBORenderTexture::stop(){	
	// Stop acquiring and unbind the FBO
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glPopAttrib();
}

void FBORenderTexture::showTexture(unsigned int i, float fSizeX, float fSizeY, float x, float y) const {
	GLuint texture = m_diffuseTexture;
	if(i == 1) {
		texture = m_positionTexture;
	} else if(i == 2) {
		texture = m_normalsTexture;
	}
	
	//Projection setup
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(0,m_width,0,m_height,0.1f,2);	

	//Model setup
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();

	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);

	// Render the quad
	glLoadIdentity();
	glTranslatef(x,-y,-1.0);
	
	glColor3f(1,1,1);
	glBegin(GL_QUADS);
		glTexCoord2f( 0, 1 );
		glVertex3f(0.0f,  (float) m_height, 0.0f);
		glTexCoord2f( 0, 0 );
		glVertex3f(0.0f,   m_height-fSizeY, 0.0f);
		glTexCoord2f( 1, 0 );
		glVertex3f(fSizeX,  m_height-fSizeY, 0.0f);
		glTexCoord2f( 1, 1 );
		glVertex3f(fSizeX, (float) m_height, 0.0f);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

	//Reset to the matrices	
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}
#pragma once
#include <windows.h>
#include "GL/glew.h"
#include "GL/gl.h"
#include "GL/freeglut.h"
#include "../SimpleGLShader.h"
#include "FBORenderTexture.h"

class DeferredRendering
{
public:
	DeferredRendering(int w, int h, FBORenderTexture* m_fboRT);

	void render();

	void resize(int w, int h) { 
		// TODO
	}
private:
	SimpleGLShader 		m_shader;		// Deferred rendering shader
	FBORenderTexture*	m_fboRT;		// A pointer to the FBO render texture that contains diffuse, normals and positions

	unsigned int		m_width;
	unsigned int		m_height;

	char*				m_diffuseID;	// Diffuse texture handle for the shader
	char*				m_positionID;	// Position texture handle for the shader
	char*				m_normalsID;	// Normals texture handle for the shader
	char*				m_lightPosition;// Light position handle for the shader
	char* m_viewProj;
};

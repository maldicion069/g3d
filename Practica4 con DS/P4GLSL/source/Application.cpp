#include "Application.h"

Application* Application::instance = NULL;

glm::vec3& Application::lightPosition() { 
	return m_lightPos; 
}

void Application::showDeferredRendering() { 
	m_state = 0; 
}

void Application::showRenderTargets() { 
	m_state = 1; 
}

void Application::reshape_(int w, int h) {
	Application::getInstance()->setSize(w, h);
}

void Application::render_() {
	Application::getInstance()->render();
}

void Application::update_() {
	Application::getInstance()->update();
}

void Application::key_(unsigned char key, int x, int y) {
	Application::getInstance()->keys(key, x, y);
}

Application* Application::getInstance() {
	if(!instance) {
		instance = new Application();
	}
    return instance;
}

bool Application::initialize(int argc, char** argv, int w, int h, char* title) {
	glutInit(&argc, argv);

	m_windowWidth = w;
	m_windowHeight = h;

	glutInitContextVersion(2, 1);
	//glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	//glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(w, h);
	glutCreateWindow(title);
	glutDisplayFunc(&Application::render_);
	glutIdleFunc(&Application::update_);
	glutReshapeFunc(&Application::reshape_);
	glutKeyboardFunc(&Application::key_);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_DEPTH_TEST);

	glewExperimental=TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		return false;
	}
	
	m_lightPos = glm::vec3(-1.2f, 2.0f, -7.4f);

	try {
		loadAssets();
	} catch (std::exception& e) {
		return false;
	}

	return true;
}

void Application::keys(unsigned char key, int x, int y) {
	switch(key) {
		case 'a':
			showDeferredRendering();
			break;
		case 's':
			showRenderTargets();
			break;
		case 't':
			m_lightPos.y += 0.2f;
			break;
		case 'f':
			m_lightPos.x -= 0.2f;
			break;
		case 'g':
			m_lightPos.x += 0.2f;
			break;
		case 'v':
			m_lightPos.y -= 0.2f;
			break;
		case 'r':
			m_lightPos.z -= 0.2f;
			break;
		case 'y':
			m_lightPos.z += 0.2f;
			break;
	}
	std::cout << m_lightPos[0] << ", " << m_lightPos[1] << ", " << m_lightPos[2] << std::endl;
}
void Application::run() {
    glutMainLoop();
}

void Application::setSize(int w, int h) {
	m_windowWidth = w;
	m_windowHeight = h;

	m_deferS->resize(w, h);
	m_mRT->resize(w, h);

	glViewport(0, 0, m_windowWidth, m_windowHeight);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	Constants::proj = glm::perspective(glm::radians(45.0f), float(m_windowWidth) / float(m_windowHeight), 0.1f, 100.0f);
	glMultMatrixf(glm::value_ptr(Constants::proj));

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Application::update() {
    int timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
    int deltaTime = timeSinceStart - oldTimeSinceStart;
    oldTimeSinceStart = timeSinceStart;

	float time = /*deltaTime**/0.6f;

 	m_models[0]->addRotation( time, time*2, 0 );
 	m_models[1]->addRotation( 0, time*2, time );
 	m_models[2]->addRotation( 0, time, 0 );
 	m_models[3]->addRotation( 0, -time*5,  0);
	glutPostRedisplay();
}

void Application::render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.2f, 0.3f, 0.8f, 1.0f);

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	glm::mat4 modelview = glm::mat4(1.0f);
	modelview = glm::rotate(modelview, D2R*20.0f, glm::vec3(1, 0, 0));
	modelview = glm::translate(modelview, glm::vec3(0.0f,-4.6f,-10.0f));

	glMultMatrixf(glm::value_ptr(modelview));

	// Render our geometry into the FBO
	m_mRT->start();
	for(std::vector<Model*>::iterator it = m_models.begin(); it != m_models.end(); ++it) {
		(*it)->render();
	}
	m_mRT->stop();

	// Render to the screen
	if(m_state == 0) {
		m_deferS->render();	// Render to screen using the deferred rendering shader
	}
	else if(m_state == 1) {
		m_mRT->showTexture( 0, 512, 384, 0);
		m_mRT->showTexture( 1, 512, 384, 512);
		m_mRT->showTexture( 2, 512, 384, 0, 384);
	}

	glutSwapBuffers();
}

void Application::release() {
	releaseAssets();
}

void Application::loadAssets() {
	m_state = 0;

	m_mRT = new FBORenderTexture(m_windowWidth, m_windowHeight);
	m_deferS = new DeferredRendering(m_windowWidth, m_windowHeight, m_mRT);

	Model* cube = new CubeModel("data/deferredShading.vert", "data/deferredShading.frag", 1);
	cube->loadTexture("data/box.raw");
	cube->setPosition(2,2.0f,3.0f);
	m_models.push_back(cube);
	
	Model* sphere = new SphereModel("data/deferredShading.vert", "data/deferredShading.frag", 1, 8);
	sphere->loadTexture("data/earth.raw");
	sphere->setPosition(-2,2.5f,0);
	m_models.push_back(sphere);

	Model* plane = new PlaneModel("data/deferredShading.vert", "data/deferredShading.frag", 5);
	plane->loadTexture("data/plane.raw");
	plane->setPosition(0,0,0);
	m_models.push_back(plane);

	Model* tube = new TubeModel("data/deferredShading.vert", "data/deferredShading.frag", 0.5f, 0.5f, 2.5f, 65, 50);
	tube->loadTexture("data/tube.raw");
	tube->setPosition(-2,0.7f,1.0);
	tube->setRotation(0, 90, 0);
	m_models.push_back(tube);
}

void Application::releaseAssets() {
	delete m_mRT;
	delete m_deferS;

	size_t size = m_models.size();
	for(int i = 0; i < size; i++) {
		delete m_models[i];
	}
}

#include "Application.h"

int main(int argc, char** argv) {
	Application* app = Application::getInstance();
	if(app->initialize(argc, argv, 1024, 768, "Deferred Shading")) {
		app->run();
		app->release();
	}
	return 0;
}
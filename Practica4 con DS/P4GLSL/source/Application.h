#pragma once

#include "DeferredRendering/DeferredRendering.h"
#include "DeferredRendering/FBORenderTexture.h"
#include "Model/SphereModel.h"
#include "Model/CubeModel.h"
#include "Model/PlaneModel.h"
#include "Model/TubeModel.h"
#include "GL/glew.h"
#include "GL/gl.h"
#include "GL/freeglut.h"
#include "Constants.h"

#include <iostream>

class Application {
public:
	bool initialize(int argc, char** argv, int w, int h, char* title);
	void run();
	void release();

    static Application* getInstance();

	glm::vec3& lightPosition();
private:
    static Application* instance;
	
	void loadAssets();
	void releaseAssets();
	
	void setSize(int w, int h);
	void update();
	void render();

	void showDeferredRendering();
	void showRenderTargets();
	
	static void reshape_(int w, int h);
	static void render_();
	static void update_();
	static void key_(unsigned char key, int x, int y);

	void keys(unsigned char key, int x, int y);

	std::vector<Model*>	m_models;
	DeferredRendering*	m_deferS;
	FBORenderTexture*	m_mRT;

	int					m_windowWidth;
	int					m_windowHeight;

	unsigned int		oldTimeSinceStart;
	unsigned char		m_state; // 0 - Normal render, 1 - Show render targets

	glm::vec3			m_lightPos;
};

#version 330 core

//Shader para simular la luz Focal

out vec4 outColor;

in vec3 color;
in vec3 pos;
in vec3 norm;
in vec2 texCoord;

uniform mat4 normal;
uniform mat4 modelView;
uniform sampler2D colorTex;
uniform sampler2D emiTex;
uniform sampler2D specularTex;

struct LightInfo {
	vec3 Position;
	vec3 La;			// Ambient light intensity
	vec3 Ld;			// Diffuse light intensity
	vec3 Ls;			// Specular light intensity
};
uniform LightInfo Light;
uniform vec3 lightDir;

//Material
struct MaterialInfo {
	vec3 Ka;
	vec3 Kd;
	vec3 Ks;
	vec3 Ke;
	vec3 N;
};
MaterialInfo Material;

uniform float n;

float spotExponent = 1.50;

uniform float cutOff;			// angle
uniform vec4 csAndDiffs;

float calculateAttenuation(float spotEffect);
vec4 spotlight();

void main() {
	Material.Ka = texture(colorTex, texCoord).rgb;
	Material.Kd = Material.Ka;
	Material.Ks = texture(specularTex, texCoord).rgb;
	Material.Ke = texture(emiTex, texCoord).rgb;

	Material.N = normalize(norm);
	outColor = spotlight();
}

vec4 spotlight(){
	vec3 ambientColor = Light.La*Material.Ka;
	float diffuse = max(0, dot(Material.N, lightDir));

	vec3 L = (Light.Position-pos);
	float d = length(L);
	L = normalize(L);

	//Intensidad luminica, coeficiente de iluminacion difusa...y producto escalar N y L.
	vec3 diffuseColor = clamp(Light.Ld * Material.Kd * dot(Material.N,L), 0, 1);

	vec3 R = reflect(-L, Material.N); // R is the reflected ray
	vec3 V = normalize(-pos); // V is the ray that goes from the point to the camera

	float specularFactor = max(dot(R,V), 0.0001); 
	specularFactor = pow(specularFactor, n); // Careful! If x <= 0 the result is undefined!!
	vec3 specularColor = Light.Ls * Material.Ks * specularFactor;

	float spotEffect = dot(lightDir, -L);

	if(spotEffect > cutOff){
		
		diffuseColor *= calculateAttenuation(spotEffect);

		// Custom purple color in spotlight
		diffuseColor *= vec3(0.5, 0.2, 0.8);
		
		return vec4(ambientColor + diffuseColor + specularColor + Material.Ke, 1.0);
	}
	return vec4(Material.Ke + ambientColor, 1.0);
}

float calculateAttenuation(float spotEffect) {
	if(csAndDiffs.w > 0.0) {
		float spotlightLighting = pow(((spotEffect - cutOff)/ (1.0 - cutOff)), spotExponent);
		return spotlightLighting;
	}
	return 1.0;
}
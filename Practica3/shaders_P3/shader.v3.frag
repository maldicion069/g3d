#version 330 core

subroutine vec4 shadeModelType();
subroutine uniform shadeModelType shadeModel;

out vec4 outColor;

in vec3 color;
in vec3 pos;
in vec3 norm;
in vec2 texCoord;

uniform sampler2D colorTex;
uniform sampler2D emiTex;
uniform sampler2D espTex; // TODO: Ponerlo todo en ingles, anda ...

uniform vec3 lightPos;
uniform vec3 lightDir;
uniform float cutOff; // angle
uniform vec4 csAndDiffs;

//Propiedades del objeto
vec3 Ka;
vec3 Kd;
vec3 Ks;
vec3 N;
float alpha = 5000.0;
vec3 Ke;

// TODO: Esto deber�a ir fuera, no. No s� ni qu� es
float n = 30.0;

//Propiedades de la luz
uniform vec3 Ia;
uniform vec3 Id;
uniform vec3 Is;
uniform vec3 Il;

float calculateAttenuation(float d);

void main() {
	Ka = texture(colorTex, texCoord).rgb;
	Kd = texture(colorTex, texCoord).rgb;
	Ke = texture(emiTex, texCoord).rgb;
	Ks = texture(espTex, texCoord).rgb; // TODO: Preguntar si est� bien as� ... //vec3 (1.0);

	N = normalize (norm);
	
	outColor = shadeModel(); //vec4(vec3(Ks), 1.0); // shadeModel();//vec4(shade(), 1.0);   
}

float calculateAttenuation(float d) {
	if(csAndDiffs.w > 0.0) {
		float factorAtenuacion = min(1.0/(csAndDiffs.x+csAndDiffs.y*d+csAndDiffs.z*d*d),1.0);
		return factorAtenuacion;
	}
	return 1.0;
}

// Implementar luz puntual
subroutine( shadeModelType )
vec4 point() {
	vec3 c = vec3(0.0);
	c = Ia * Ka;
	
	vec3 L = (lightPos-pos);
	float d = length(L);
	L = normalize(L);
	vec3 diffuse = (Id * Kd * dot (L,N));
	c += clamp(diffuse, 0.0, 1.0);
	
	diffuse *= calculateAttenuation(d);

	vec3 V = normalize (-pos);
	vec3 R = normalize (reflect (-L,N));
	float factor = max (dot (R,V), 0.01);
	vec3 specular = Is*Ks*pow(factor,alpha);
	c += clamp(specular, 0.0, 1.0);

	c+=Ke;
	return vec4(c, 1.0);
}

// Implementar luz focal
subroutine( shadeModelType )
vec4 spotlight() {
	vec3 colorAmbiental = Ia*Ka;
	float difuso = max(0, dot(N, lightDir));

	vec3 L = (lightPos-pos);
	float d = length(L);
	L = normalize(L);

	vec3 inversaL = -L;
	float efectoFoco = dot(inversaL, lightDir);
	
	if(efectoFoco > cutOff) {
		//Intensidad luminica, coeficiente de iluminacion difusa...y producto escalar N y L.
		vec3 colorDifuso = clamp(Il * Kd * dot(N,L), 0, 1);
		
		colorDifuso *= calculateAttenuation(d);

		vec3 R = reflect(-L, N); //R es el rayo reflejado. Si L y N est�n normalizados no hace falta normalizarlo
		vec3 V = normalize(-pos); //V es el rayo que va desde el punto hasta la c�mara
		float factorEspecular = max(dot(R,V),0.0001); 
		factorEspecular = pow(factorEspecular, n); //Ojo! si x <= 0 el resultado es indefinido!!
		vec3 colorEspecular = Il * Ks * factorEspecular;
	
		return difuso * vec4(colorAmbiental + colorDifuso + colorEspecular + Ke, 1.0);
	}
	return vec4(Ke, 1.0);
}

// Implementar luz direccional
subroutine( shadeModelType )
vec4 directional() {
	vec3 colorAmbiental = Ia*Ka;
	vec3 L = (lightPos-pos);
	float d = length(L);
	L = normalize(L);
	//Intensidad luminica, coeficiente de iluminacion difusa...y producto escalar N y L.
	vec3 colorDifuso = clamp(Il * Kd * dot(N,L), 0, 1) * vec3(0.5, 0.2, 0.8);
	float difuso = max(0, dot(N, lightDir));
	
	colorDifuso *= calculateAttenuation(d);

	vec3 R = reflect(-L, N); //R es el rayo reflejado. Si L y N est�n normalizados no hace falta normalizarlo
	vec3 V = normalize(-pos); //V es el rayo que va desde el punto hasta la c�mara
	float factorEspecular = max(dot(R,V),0.0001); 
	factorEspecular = pow(factorEspecular, n); //Ojo! si x <= 0 el resultado es indefinido!!
	vec3 colorEspecular = Il * Ks * factorEspecular;

	return difuso*vec4((colorAmbiental + colorDifuso + colorEspecular + Ke), 1.0);
}
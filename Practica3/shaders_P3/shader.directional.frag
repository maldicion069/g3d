#version 330 core

out vec4 outColor;

in vec3 color;
in vec3 pos;
in vec3 norm;
in vec2 texCoord;

struct MaterialInfo {
	vec3 Ka;
	vec3 Kd;
	vec3 Ks;
	vec3 Ke;
	vec3 N;
};
MaterialInfo Material;

struct LightInfo {
	vec3 Position;
	vec3 La;			// Ambient light intensity
	vec3 Ld;			// Diffuse light intensity
	vec3 Ls;			// Specular light intensity
};
uniform LightInfo Light;

uniform sampler2D colorTex;
uniform sampler2D emiTex;
uniform sampler2D specTex;

uniform vec3 lightDir;
uniform float cutOff; // angle
uniform vec4 csAndDiffs;

uniform float n;

float calculateAttenuation(float d);
vec4 directional();

void main() {
	Material.Ka = texture(colorTex, texCoord).rgb;
	Material.Kd = Material.Ka;
	Material.Ke = texture(emiTex, texCoord).rgb;
	Material.Ks = texture(specTex, texCoord).rgb;

	Material.N = normalize (norm);
	
	outColor = directional();
}

float calculateAttenuation(float d) {
	if(csAndDiffs.w > 0.0) {
		float factorAtenuacion = min(1.0/(csAndDiffs.x+csAndDiffs.y*d+csAndDiffs.z*d*d),1.0);
		return factorAtenuacion;
	}
	return 1.0;
}

vec4 directional() {
	vec3 finalColor = Material.Ke;

	// Add ambiental color
	finalColor += Light.La*Material.Ka;

	vec3 L = (Light.Position-pos);
	float d = length(L);
	L = normalize(L);

	// Calculate diffuse color
	vec3 diffuseColor = clamp(Light.Ld * Material.Kd * dot(Material.N,L), 0, 1) * vec3(0.5, 0.2, 0.8);
	float diffuse = max(0, dot(Material.N, lightDir));
	
	diffuseColor *= calculateAttenuation(d) * diffuse;

	// Add diffuse color
	finalColor += diffuseColor;

	vec3 R = reflect(-L, Material.N);					// R is the reflected ray
	vec3 V = normalize(-pos);							// V is the ray that goes from the point to the camera
	float specFactor = max(dot(R,V), 0.0001); 
	specFactor = pow(specFactor, n);					// Careful! If x <= 0 the result is undefined!!
	vec3 specularColor = Light.Ls * Material.Ks * specFactor;

	// Add specular color
	finalColor += Light.Ls * Material.Ks * specFactor;

	return vec4(finalColor, 1.0);
}
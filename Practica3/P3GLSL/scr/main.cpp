﻿#include "BOX.h"
#include "auxiliar.h"

#include <windows.h>

#include <gl/glew.h>
#include <gl/gl.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h> 
#include <iostream>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "SimpleGLShader.h"
#include "SimpleGLTexture.h"
#include <sstream>

std::vector<SimpleGLShader> shaders;
SimpleGLShader* usedShader;

int oldTimeSinceStart = 0;

//////////////////////////////////////////////////////////////
// Datos que se almacenan en la memoria de la CPU
//////////////////////////////////////////////////////////////

//Matrices
glm::mat4	proj = glm::mat4(1.0f);
glm::mat4	view = glm::mat4(1.0f);
glm::mat4	model = glm::mat4(1.0f);
glm::mat4	model2 = glm::mat4(1.0f);

typedef struct SubRoutinesLights {
	glm::vec3 lightPos;
	glm::vec3 lightDir;
	float cutOff;

	glm::vec3 Ia;
	glm::vec3 Id;
	glm::vec3 Is;
	glm::vec3 Il;

	glm::vec4 csAndDiffs; // vec4(c1, c2, c3, activeDiff) // activeDiff == 0 ? true / false

	float n;
} SubRoutinesLights;

glm::vec3* Intensity_selector;

SubRoutinesLights subrouts;

//////////////////////////////////////////////////////////////
// Variables que nos dan acceso a Objetos OpenGL
//////////////////////////////////////////////////////////////

//Texturas
SimpleGLTexture colorTex;
SimpleGLTexture emiTex;
SimpleGLTexture specTex;

//Por definir
//VAO
unsigned int vao;
//VBOs que forman parte del objeto
unsigned int posVBO;
unsigned int colorVBO;
unsigned int normalVBO;
unsigned int texCoordVBO;
unsigned int triangleIndexVBO;

//////////////////////////////////////////////////////////////
// Funciones auxiliares
//////////////////////////////////////////////////////////////
//!!Por implementar

//Declaración de CB
void renderFunc();
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mouseMotionFunc(int x, int y);

// Keyboard events
void modify_light_position(unsigned char key);
void modify_light_intensity(unsigned char key);
void modify_cutoff(unsigned char key);
void modify_light_selection(unsigned char key);
void modify_camera_move(unsigned char key);

//Funciones de inicialización y destrucción
void initContext(int argc, char** argv);
void initOGL();
void initShader(const char *vname, const char *fname);
void initObj();
void destroy();
void changeTitle();

std::string title = "Luz puntual";

std::vector<std::string> titleName(3);	// 0: Type
										// 1: Att
										// 2: Intensity to modify

float fLargest = -1;

template <typename T>
T clip(const T& n, const T& lower, const T& upper) {
	return std::max(lower, std::min(n, upper));
}

int main(int argc, char** argv) {
	std::locale::global(std::locale("spanish"));// acentos ;)

	titleName[0] = "Point";
	titleName[1] = "without";
	titleName[2] = "";

	initContext(argc, argv);
	initOGL();
	initShader("../shaders_P3/shader.point.vert", "../shaders_P3/shader.point.frag");
	initShader("../shaders_P3/shader.spotlight.vert", "../shaders_P3/shader.spotlight.frag");
	initShader("../shaders_P3/shader.directional.vert", "../shaders_P3/shader.directional.frag");
		
	//if(glewIsSupported("GL_EXT_texture_filter_anisotropic")) {
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
		printf("App::Load > %.0fx Anistropic filtering supported.", fLargest);
	//} else {
	//	printf("App::Load > Anistropic filtering NOT supported.");
	//	fLargest = -1;
	//}

	subrouts.lightPos = glm::vec3(0.0, 0.0, +3.0);
	subrouts.lightDir = glm::vec3(0.0, 0.0, -1.0);
	subrouts.cutOff = 40.0f; // Cuidado al enviar al shader!!//glm::cos(glm::radians(40.0));
	subrouts.csAndDiffs = glm::vec4(
		1.0,
		0.0,
		0.1,
		0.0
	);
	subrouts.Ia = glm::vec3 (0.15);
	subrouts.Id = glm::vec3 (1.0);
	subrouts.Is = glm::vec3 (0.5);
	subrouts.Il = glm::vec3 (0.8);

	subrouts.n = 30.0f;

	//Intensity_selector = &subrouts.Ia;
	usedShader = &shaders[1];

	initObj();
	glutMainLoop(); //Escucha eventos y redirige
	destroy();

	return 0;
}
	
//////////////////////////////////////////
// Funciones auxiliares 
void initContext(int argc, char** argv) {
	glutInit(&argc,argv);
	glutInitContextVersion(3,3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH); 
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("");
	changeTitle();

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit (-1);
	}
	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;

	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(mouseMotionFunc);
}

void initOGL() {
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	//Habilitamos el culling
	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	proj = glm::perspective(glm::radians(60.0f), 1.0f, 0.1f, 50.0f);
	view = glm::mat4(1.0f);
	view[3].z = -6;
}

void destroy() {
	for(std::vector<SimpleGLShader>::iterator shader = shaders.begin(); shader != shaders.end(); ++shader) {
		shader->destroy();
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	for(int i = 0; i < shaders.size(); i++){
		if (shaders[i].attribute("inPos") != -1) glDeleteBuffers(1, &posVBO);
		if (shaders[i].attribute("inColor") != -1) glDeleteBuffers(1, &colorVBO);
		if (shaders[i].attribute("inNormal") != -1) glDeleteBuffers(1, &normalVBO);
		if (shaders[i].attribute("inTexCoord") != -1) glDeleteBuffers(1, &texCoordVBO);
	}

	colorTex.deleteTexture();
	emiTex.deleteTexture();
	specTex.deleteTexture();

	glDeleteBuffers(1, &triangleIndexVBO);
	glBindVertexArray(0);
	glDeleteVertexArrays(1, &vao);

}

void initShader(const char *vname, const char *fname) {
	SimpleGLShader sgls;
	sgls.load(vname, GL_VERTEX_SHADER);
	sgls.load(fname, GL_FRAGMENT_SHADER);
	
	sgls.create();
	sgls.link();

	sgls.use();

	sgls.add_uniform("normal");
	sgls.add_uniform("modelView");
	sgls.add_uniform("modelViewProj");
	sgls.add_uniform("colorTex");
	sgls.add_uniform("emiTex");
	sgls.add_uniform("specTex");
	sgls.add_uniform("Light.Position");
	sgls.add_uniform("lightDir");
	sgls.add_uniform("cutOff");
	sgls.add_uniform("csAndDiffs");
	sgls.add_uniform("n");

	sgls.add_uniform("Light.La");
	sgls.add_uniform("Light.Ld");
	sgls.add_uniform("Light.Ls");
	//sgls.add_uniform("Il");
	
	sgls.add_attribute("inPos");
	sgls.add_attribute("inColor");
	sgls.add_attribute("inNormal");
	sgls.add_attribute("inTexCoord");

	sgls.unuse();

	shaders.push_back(sgls);
}

void changeTitle() {
	std::stringstream ss;
	ss << titleName[0];
	ss << " light ";
	ss << titleName[1];
	ss << " attenuation";
	if(Intensity_selector) {
		ss << " [";
		ss << titleName[2];
		ss << "=>";
		ss << "(" << Intensity_selector->x << ", " << Intensity_selector->y << ", " << Intensity_selector->z << ")]";
	}
	glutSetWindowTitle(ss.str().c_str());
}

void initObj() {
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	
	for(std::vector<SimpleGLShader>::iterator shader = shaders.begin(); shader != shaders.end(); ++shader) {
		if (shader->attribute("inPos") != -1){
			glGenBuffers(1, &posVBO);
			glBindBuffer(GL_ARRAY_BUFFER, posVBO);
			glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexPos, GL_STATIC_DRAW);
			glVertexAttribPointer(shader->attribute("inPos"), 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(shader->attribute("inPos"));
		}

		if (shader->attribute("inColor") != -1){
			glGenBuffers(1, &colorVBO);
			glBindBuffer(GL_ARRAY_BUFFER, colorVBO); //GL_ARRAY_BUFFERS para atributos
			glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexColor, GL_STATIC_DRAW);
			glVertexAttribPointer(shader->attribute("inColor"), 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(shader->attribute("inColor"));
		}

		if (shader->attribute("inNormal") != -1){
			glGenBuffers(1, &normalVBO);
			glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
			glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexNormal, GL_STATIC_DRAW);
			glVertexAttribPointer(shader->attribute("inNormal"), 3, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(shader->attribute("inNormal"));
		}

		if (shader->attribute("inTexCoord") != -1){
			glGenBuffers(1, &texCoordVBO);
			glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
			glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 2, cubeVertexTexCoord, GL_STATIC_DRAW);
			glVertexAttribPointer(shader->attribute("inTexCoord"), 2, GL_FLOAT, GL_FALSE, 0, 0);
			glEnableVertexAttribArray(shader->attribute("inTexCoord"));
		}
	}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, cubeNTriangleIndex*sizeof(unsigned int) * 3, cubeTriangleIndex, GL_STATIC_DRAW);

	model = glm::mat4(1.0f);
	model2 = glm::mat4(1.0f);

	colorTex.load("../img/color2.png", fLargest);
	emiTex.load("../img/emissive.png", fLargest);
	specTex.load("../img/specMap.png", fLargest);
}

void renderFunc() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	usedShader->use();
	
	//Texturas
	if (usedShader->uniform("colorTex") != -1) {
		colorTex.select_and_bind2DTexture();
		glUniform1i(usedShader->uniform("colorTex"), 0);
	}
	if (usedShader->uniform("emiTex") != -1) {
		emiTex.select_and_bind2DTexture();
		glUniform1i(usedShader->uniform("emiTex"), 1);
	}
	if (usedShader->uniform("specTex") != -1) {
		specTex.select_and_bind2DTexture();
		glUniform1i(usedShader->uniform("specTex"), 2);
	}

	//Pintado del objeto!!!! 
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));
	if (usedShader->uniform("modelView") != -1) {
		glUniformMatrix4fv(usedShader->uniform("modelView"), 1, GL_FALSE,&(modelView[0][0]));
	}
	if (usedShader->uniform("modelViewProj") != -1) {
		glUniformMatrix4fv(usedShader->uniform("modelViewProj"), 1, GL_FALSE, &(modelViewProj[0][0]));
	}
	if (usedShader->uniform("normal") != -1) {
		glUniformMatrix4fv(usedShader->uniform("normal"), 1, GL_FALSE,&(normal[0][0]));
	}

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex*3,GL_UNSIGNED_INT, (void*)0);

	// Pintado model2
	

	//Pintado del objeto!!!! 
	modelView = view * model2;
	modelViewProj = proj * view * model2;
	normal = glm::transpose(glm::inverse(modelView));
	if (usedShader->uniform("modelView") != -1) {
		glUniformMatrix4fv(usedShader->uniform("modelView"), 1, GL_FALSE,&(modelView[0][0]));
	}
	if (usedShader->uniform("modelViewProj") != -1) {
		glUniformMatrix4fv(usedShader->uniform("modelViewProj"), 1, GL_FALSE, &(modelViewProj[0][0]));
	}
	if (usedShader->uniform("normal") != -1) {
		glUniformMatrix4fv(usedShader->uniform("normal"), 1, GL_FALSE,&(normal[0][0]));
	}

	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex*3,GL_UNSIGNED_INT, (void*)0);
	
	if (usedShader->uniform("Light.Position") != -1) {
		glm::vec4 aux = view * glm::vec4(subrouts.lightPos, 1.0f);
		glm::vec3 lightPos = glm::vec3(aux.x, aux.y, aux.z);
		glUniform3fv(usedShader->uniform("Light.Position"), 1, &(lightPos.x));
	}
	if (usedShader->uniform("lightDir") != -1) {
		glUniform3fv(usedShader->uniform("lightDir"), 1, &(subrouts.lightDir.x));
	}
	if (usedShader->uniform("cutOff") != -1) {
		glUniform1f(usedShader->uniform("cutOff"), glm::cos(glm::radians(subrouts.cutOff)));
	}
	if (usedShader->uniform("csAndDiffs") != -1) {
		glUniform4fv(usedShader->uniform("csAndDiffs"), 1, &(subrouts.csAndDiffs.x));
	}
	if (usedShader->uniform("n") != -1) {
		glUniform1f(usedShader->uniform("n"), subrouts.n);
	}

	/*if (usedShader->uniform("Il") != -1) {
		glUniform3fv(usedShader->uniform("Il"), 1, &(subrouts.Il.x));
	}*/

	if (usedShader->uniform("Light.La") != -1) {
		glUniform3fv(usedShader->uniform("Light.La"), 1, &(subrouts.Ia.x));
	}
	if (usedShader->uniform("Light.Ld") != -1) {
		glUniform3fv(usedShader->uniform("Light.Ld"), 1, &(subrouts.Id.x));
	}
	if (usedShader->uniform("Light.Ls") != -1) {
		glUniform3fv(usedShader->uniform("Light.Ls"), 1, &(subrouts.Is.x));
	}

	usedShader->unuse();

	glutSwapBuffers();
}

void resizeFunc(int width, int height) {
	//Para ajustar el aspect ratio hay que usar la matriz projection.
	glViewport(0, 0, width, height);

	proj = glm::perspective(glm::radians(60.0f), float(width) /float(height), 1.0f, 50.0f);
}

void idleFunc() {
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
    int dt = currentTime - oldTimeSinceStart;
    oldTimeSinceStart = currentTime;

	static float angle = 0.0f;
	static float angle_orbital = 0.1f;

	model = glm::mat4(1.0f);
	angle = (angle > 3.141592f * 2.0f) ? 0 : angle + 0.001f*dt;
	model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 0.0f));
	
	model2 = glm::mat4 (1.0f);
	angle_orbital = (angle_orbital > 2.0f*3.14159f) ? 0.0f:angle_orbital+0.001f*dt;
	model2 = glm::rotate(model2, angle_orbital, glm::vec3(0.0f,1.0f,0.0f));
	model2 = glm::translate(model2, glm::vec3(10.0f,0.0f,0.0f));

	glutPostRedisplay();
}

void modify_light_position(unsigned char key) {
	switch (key) {
		case 'a':
			subrouts.lightPos.x -= 1.1f;
			break;
		case 's':
			subrouts.lightPos.x = 0.0f;
			subrouts.lightPos.y = 0.0f;
			subrouts.lightPos.z = 0.0f;
			break;
		case 'd':
			subrouts.lightPos.x += 1.1f;
			break;
		case 'w':
			subrouts.lightPos.y += 1.1f;
			break;
		case 'x':
			subrouts.lightPos.y -= 1.1f;
			break;
		case 'q': subrouts.lightPos.z -= 1.1f;
			break;
		case 'z': subrouts.lightPos.z += 1.1f;
			break;
		case 'c':
			subrouts.n += 1.0f;
			subrouts.n = clip<float>(subrouts.n, 0, 180);
			break;
		case 'v':
			subrouts.n -= 1.0f;
			subrouts.n = clip<float>(subrouts.n, 0, 180);
			break;
	}
}

void modify_light_intensity(unsigned char key) {
	switch (key) {
		// Seleccionar Intensidad de luz
		case 'y':
			Intensity_selector = &subrouts.Ia;
			titleName[2] = "Ia";
			break;
		case 'u':
			Intensity_selector = &subrouts.Id;
			titleName[2] = "Id";
			break;
		case 'i':
			Intensity_selector = &subrouts.Is;
			titleName[2] = "Is";
			break;
		/*case 'o':
			Intensity_selector = &subrouts.Il;
			break;*/
		case '+':
			if(Intensity_selector) {
				*Intensity_selector += 0.1f;
				*Intensity_selector = glm::vec3(clip(Intensity_selector->x, 0.0f, 1.0f));
			}
			break;
		case '-':
			if(Intensity_selector) {
				*Intensity_selector -= 0.1f;
				*Intensity_selector = glm::vec3(clip(Intensity_selector->x, 0.0f, 1.0f));
			}
			break;
	}
}

void modify_cutoff(unsigned char key) {
	switch(key) {
		case 'r': {
			subrouts.cutOff -= 0.5f;
			subrouts.cutOff = clip<float>(subrouts.cutOff, 0, 180);
			break;
		}
		case 't': {
			subrouts.cutOff += 0.5f;
			subrouts.cutOff = clip<float>(subrouts.cutOff, 0, 180);
			break;
		}
	}
}

void modify_light_selection(unsigned char key) {
	switch(key) {
		case 'b': {
			usedShader = &shaders[0];
			titleName[0] = "Point";
			break;
		}
		case 'n': {
			usedShader = &shaders[1];
			titleName[0] = "Spotlight";
			break;
		}
		case 'm': {
			usedShader = &shaders[2];
			titleName[0] = "Directional";
			break;
			}
		case 'p': {
			if(subrouts.csAndDiffs.w == 0.0f) { // Atenuación
				subrouts.csAndDiffs.w = 1.0f;
				titleName[1] = "with";
			} else {
				subrouts.csAndDiffs.w = 0.0f;
				titleName[1] = "without";
			}
			break;
		}
	}
}

void modify_camera_move(unsigned char key) {
	const float MOVEMENT_SPEED = 1.0f;
	const float ANGLE_ROTATION = 0.01f;

	switch (key) {
		// MOVIMIENTOS CÁMARA
		case '6': //Right movement
			view = glm::translate(view, glm::vec3(1.0 * MOVEMENT_SPEED, 0.0f, 0.0f));
			break;
		
		case '4': //Left movement
			view = glm::translate(view, glm::vec3(1.0 * -MOVEMENT_SPEED, 0.0f, 0.0f));
			break;
		
		case '8': //Up movement
			view = glm::translate(view, glm::vec3(0.0f, 1.0f * MOVEMENT_SPEED, 0.0f));
			break;
		
		case '2': //Bottom movement
			view = glm::translate(view, glm::vec3(0.0f, 1.0f * -MOVEMENT_SPEED, 0.0f));
			break;
		case '7': // Girar izquierda
			view = glm::rotate(view, ANGLE_ROTATION, glm::vec3(0.0f, 1.0f, 0.0f));
		case '9': // Girar derecha
			view = glm::rotate(view, -ANGLE_ROTATION, glm::vec3(0.0f, 1.0f, 0.0f));
		case '1': // - Zoom
			view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f * -MOVEMENT_SPEED));
			break;
		case '3': // + Zoom
			view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f * MOVEMENT_SPEED));
			break;
	}
}

void keyboardFunc(unsigned char key, int x, int y) {
	std::cout << "Se ha pulsado la tecla " << key << std::endl << std::endl;
	
	modify_light_position(key);
	modify_light_intensity(key);
	modify_cutoff(key);
	modify_light_selection(key);
	modify_camera_move(key);

	changeTitle();

	glutPostRedisplay();
}

bool mouse_move_active = false, is_scrolling = false;
float oldX, oldY;
void mouseFunc(int button, int state, int x, int y) {
	if (state==0) {
		mouse_move_active = true;
		if (button == 1)
			is_scrolling = true;
		oldX = x;
		oldY = y;
		std::cout << "Se ha pulsado el botón ";
	}
	else {
		mouse_move_active = false;
		if (button == 1)
			is_scrolling = false;
		std::cout << "Se ha soltado el botón ";
	}
	
	if (button == 0) std::cout << "de la izquierda del ratón " << std::endl;
	if (button == 1) std::cout << "central del ratón " << std::endl;
	if (button == 2) std::cout << "de la derecha del ratón " << std::endl;

	std::cout << "en la posición " << x << " " << y << std::endl << std::endl;
}

void mouseMotionFunc(int x, int y) {
	const float MOVEMENT_SPEED = 1.0f;
	if(mouse_move_active) {
		if(is_scrolling) {
			float moveY = y - oldY;
			moveY *= 0.1f;
			std::cout << "Nos movemos " << moveY << std::endl;
			view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f*moveY));

			oldY = y;
		} else {
			float moveX = x - oldX;
			moveX *= 0.01f;
			float moveY = y - oldY;
			moveY *= -0.01f;
			std::cout << "Nos movemos " << moveX << "-" << moveY << std::endl;
			view = glm::rotate(view, moveX, glm::vec3(1.0f, 0.0f, 0.0f));
			view = glm::rotate(view, moveY, glm::vec3(0.0f, 1.0f, 0.0f));

			oldX = x;
			oldY = y;
		}
	}
}
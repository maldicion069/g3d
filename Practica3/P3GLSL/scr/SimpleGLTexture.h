// Simplify OpenGL Textures C style interaction with C++
// Copyright (C) <2015> - Cristian Rodríguez Bernal (maldicion069)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef _SIMPLEGLTEXTURE_H_
#define _SIMPLEGLTEXTURE_H_

#include <FreeImage.h>
#define _CRT_SECURE_DEPRECATE_MEMORY
#include <memory.h>

#include <iostream>
#include "GL/glew.h"

class SimpleGLTexture {
public:
	SimpleGLTexture(void);
	~SimpleGLTexture(void);
	void load(const char *fileName, float fLargest);
	void select_and_bind2DTexture();
	void deleteTexture();

protected:
	unsigned char* load_from_file_aux(const char* fileName, unsigned int &w, unsigned int &h);

	GLuint mTexId;
};

#endif /* _SIMPLEGLTEXTURE_H_ */


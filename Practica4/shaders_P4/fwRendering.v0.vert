#version 330 core

in vec3 inPos;	
in vec3 inColor;
in vec2 inTexCoord;
in vec3 inNormal;
in vec3 inTangent;

uniform mat4 modelViewProj;
uniform mat4 modelView;
uniform mat4 normal;

out vec3 color;
out vec3 pos;
out vec3 norm;
out vec2 texCoord;

out mat3 tbn;
out vec3 eyeCoord;

void main()
{
	vec3 norm_ = normalize(vec3(normal * vec4(inNormal, 1.0)));
	vec3 tang_ = normalize(vec3(normal * vec4(inTangent, 1.0)));
	vec3 binormal = normalize(cross(norm_, tang_));
	
	tbn = mat3(
		tang_.x, binormal.x, norm_.x,
		tang_.y, binormal.y, norm_.y,
		tang_.z, binormal.z, norm_.z
	);

	eyeCoord = vec3(modelView * vec4(inPos, 1.0)) * tbn;

	color = inColor;
	texCoord = inTexCoord;
	norm = (normal * vec4(inNormal, 0.0)).xyz;
	pos = (modelView * vec4(inPos, 1.0)).xyz;
	
	gl_Position =  modelViewProj * vec4 (inPos,1.0);
}

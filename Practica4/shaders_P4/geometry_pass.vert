#version 330 

layout (location = 0) in vec3 inPos; 
layout (location = 1) in vec2 inTexCoord; 
layout (location = 2) in vec3 inNormal; 

uniform mat4 modelViewProj;
uniform mat4 modelView;
uniform mat4 normal;

out vec2 texCoord; 
out vec3 norm; 
out vec3 pos; 

void main() { 
    texCoord = inTexCoord; 
    norm = (normal * vec4(inNormal, 0.0)).xyz; 
    pos = (modelView * vec4(inPos, 1.0)).xyz;
    gl_Position = modelViewProj * vec4(inPos, 1.0);
}
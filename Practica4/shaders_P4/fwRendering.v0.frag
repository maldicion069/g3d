#version 330 core

layout(location = 0) out vec4 outColor;

in vec3 color;
in vec3 pos;
in vec3 norm;
in vec2 texCoord;


in mat3 tbn;
in vec3 eyeCoord;


uniform sampler2D colorTex;
uniform sampler2D emiTex;
uniform sampler2D specTex;
uniform sampler2D normalTex;

//Propiedades del objeto
vec3 Ka;
vec3 Kd;
vec3 Ks;
float alpha = 500.0;
vec3 Ke;

//Propiedades de la luz
vec3 Ia = vec3 (0.3);
vec3 Id = vec3 (1.0);
vec3 Is = vec3 (0.7);
vec3 lpos = vec3 (0.0); 

vec3 shade(vec3 normalMap);
void main()
{
	Ka = texture(colorTex, texCoord).rgb;
	Kd = texture(colorTex, texCoord).rgb;
	Ke = texture(emiTex, texCoord).rgb;
	Ks = texture(specTex, texCoord).rgb;	//vec3 (1.0);

	//Lookup normal map
	vec4 nm = texture(normalTex, vec2(1.0-texCoord.x, texCoord.y));
	
	//Convert[0,1] -> [-1,1]
	nm = (2.0*nm-1.0);
	nm = normalize(nm);
	
	outColor = vec4(shade(nm.xyz), 1.0);   
}

vec3 shade(vec3 normalMap)
{
	vec3 c = vec3(0.0);
	c = Ia * Ka;

	vec3 eyeCoord2 = normalize(eyeCoord);

	vec3 normalizeLightVec = normalize( (lpos-eyeCoord2) *tbn);

	vec3 L = normalize (normalizeLightVec - eyeCoord2);
	vec3 diffuse = Id * Kd * dot (L,normalMap);
	c += clamp(diffuse, 0.0, 1.0);
	
	vec3 V = normalize (-eyeCoord2);
	vec3 R = normalize (reflect (-L,normalMap));
	float factor = max (dot (R,V), 0.01);
	vec3 specular = Is*Ks*pow(factor,alpha)*tbn;
	c += clamp(specular, 0.0, 1.0);

	c+=Ke;
	
	return c;
}

#version 330 core

//Variables Variantes
in vec2 texCoord;

//Textura
uniform sampler2D colorTex;

//Color de salida
out vec4 outColor;


void main(){

	//C�digo del Shader
//	outColor = vec4(textureLod(colorTex, texCoord,0).rrr, 0.6); //Escala de gris en rojo
	outColor = vec4(textureLod(colorTex, texCoord,0).rgb, 0.56); //Escala de gris en rojo
	//outColor = vec4(texCoord,vec2(1.0));
}
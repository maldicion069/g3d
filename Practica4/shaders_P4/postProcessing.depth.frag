#version 330 core

//Variables Variantes
in vec2 texCoord;

//Textura
uniform sampler2D colorTex;

uniform sampler2D vertexTex;uniform vec2 ts;uniform float fD;uniform float mD;uniform float near;uniform float far;
/*uniform float maskara[9];
uniform float mask_size;*/
//Color de salida
out vec4 outColor;

/*#define MASK_SIZE 9u
const vec2 texIdx[MASK_SIZE] = vec2[](
	vec2(-1.0,1.0), vec2(0.0,1.0), vec2(1.0,1.0),
	vec2(-1.0,0.0), vec2(0.0,0.0), vec2(1.0,1.0),
	vec2(-1.0,-1.0), vec2(0.0,-1.0), vec2(1.0,-1.0));*/

#define MASK_SIZE_ 25u
const vec2 texIdx_[MASK_SIZE_] = vec2[](
	vec2(-2.0,2.0f), vec2(-1.0,2.0f), vec2(0.0,2.0f), vec2(1.0,2.0f), vec2(2.0,2.0),
	vec2(-2.0,1.0f), vec2(-1.0,1.0f), vec2(0.0,1.0f), vec2(1.0,1.0f), vec2(2.0,1.0),
	vec2(-2.0,0.0f), vec2(-1.0,0.0f), vec2(0.0,0.0f), vec2(1.0,0.0f), vec2(2.0,0.0),
	vec2(-2.0,-1.0f), vec2(-1.0,-1.0f), vec2(0.0,-1.0f), vec2(1.0,-1.0f), vec2(2.0,-1.0),
	vec2(-2.0,-2.0f), vec2(-1.0,-2.0f), vec2(0.0,-2.0f), vec2(1.0,-2.0f), vec2(2.0,-2.0));

const float maskFactor = float (1.0/65.0);
const float mask_[MASK_SIZE_] = float[](
	1.0*maskFactor, 2.0*maskFactor, 3.0*maskFactor,2.0*maskFactor, 1.0*maskFactor,
	2.0*maskFactor, 3.0*maskFactor, 4.0*maskFactor,3.0*maskFactor, 2.0*maskFactor,
	3.0*maskFactor, 4.0*maskFactor, 5.0*maskFactor,4.0*maskFactor, 3.0*maskFactor,
	2.0*maskFactor, 3.0*maskFactor, 4.0*maskFactor,3.0*maskFactor, 2.0*maskFactor,
	1.0*maskFactor, 2.0*maskFactor, 3.0*maskFactor,2.0*maskFactor, 1.0*maskFactor);

float LinearizeDepth(float depth) {
	float z = depth * 2.0 - 1.0;
    return (2.0 * near * far) / (far + near - z * (far - near));	
}

void main() {
	float d = LinearizeDepth(texture(vertexTex, texCoord).z);
	float dof = abs(d -fD) * mD;
	dof = clamp (dof, 0.0, 1.0);
	dof *= dof;
	vec4 color = vec4 (0.0);
	for (uint i = 0u; i < MASK_SIZE_; i++) {
		vec2 iidx = texCoord + ts * texIdx_[i]*dof;
		color += texture(colorTex, iidx,0.0) * mask_[i];
	}
	outColor = color; 
	//outColor = vec4(vec3(d), 1.0);
}
#version 330 core

in vec3 inPos;	

//Variables Variantes
out vec2 texCoord;uniform float near;uniform float far;

void main() {

	//C�digo del Shader
	texCoord = inPos.xy*0.5+vec2(0.5); // Para pasar de -1 y 1 a 0 y 1
	gl_Position = vec4 (inPos,1.0);
}

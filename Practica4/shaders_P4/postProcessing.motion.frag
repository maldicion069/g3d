#version 330 core

//Variables Variantes
in vec2 texCoord;

//Textura
uniform sampler2D colorTex;

uniform float motion_param;

//Color de salida
out vec4 outColor;


void main() {
	//C�digo del Shader
	outColor = vec4(textureLod(colorTex, texCoord,0).rgb, motion_param); //Escala de gris en rojo
}
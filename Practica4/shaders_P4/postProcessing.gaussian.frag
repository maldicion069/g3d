#version 330 core

//Variables Variantes
in vec2 texCoord;

//Textura
uniform sampler2D colorTex;
uniform float maskara[9];
uniform float mask_size;uniform vec2 ts;

//Color de salida
out vec4 outColor;

/**/
#define MASK_SIZE 9u
const vec2 texIdx[MASK_SIZE] = vec2[](
	vec2(-1.0,1.0), vec2(0.0,1.0), vec2(1.0,1.0),
	vec2(-1.0,0.0), vec2(0.0,0.0), vec2(1.0,1.0),
	vec2(-1.0,-1.0), vec2(0.0,-1.0), vec2(1.0,-1.0));

void main() {
	//vec2 ts = vec2(1.0) / texSize;
	vec4 color = vec4 (0.0);
	for (uint i = 0u; i < mask_size; i++) {
		vec2 iidx = texCoord + ts * texIdx[i];
		color += texture(colorTex, iidx,0.0) * maskara[i];
	}
	outColor = color;
}
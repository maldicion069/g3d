﻿#include "BOX.h"
#include "PLANE.h"

#include <gl/glew.h>
#include <gl/gl.h>
#define SOLVE_FGLUT_WARNING
#include <gl/freeglut.h> 
#include <iostream>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include <cstdlib>
#include "SimpleGLTexture.h"

#include "Jzon.h"

#include "SimpleGLShader.h"
#include <sstream>

float fLargest;
float near_;
float far_;

#define RAND_SEED 31415926
#define SCREEN_SIZE 500,500
#define VELOCITY 0.005f

//////////////////////////////////////////////////////////////
// Datos que se almacenan en la memoria de la CPU
//////////////////////////////////////////////////////////////

//Matrices
glm::mat4	proj = glm::mat4(1.0f);
glm::mat4	view = glm::mat4(1.0f);
glm::mat4	model = glm::mat4(1.0f);

glm::vec4 blendColor(0.5f, 0.5f, 0.5f, 0.6f);
float *blend_selected;

int w, h;int idx = 0;
float oldX, oldY;

//////////////////////////////////////////////////////////////
// Variables que nos dan acceso a Objetos OpenGL
//////////////////////////////////////////////////////////////
float angle = 0.0f;

//VAO
unsigned int vao;

//VBOs que forman parte del objeto
unsigned int posVBO;
unsigned int colorVBO;
unsigned int normalVBO;
unsigned int tgVBO;
unsigned int texCoordVBO;
unsigned int triangleIndexVBO;

SimpleGLTexture colorTex;
SimpleGLTexture emiTex;
SimpleGLTexture specTex;
SimpleGLTexture normalTex;
unsigned int planeVAO;
unsigned int planeVertexVBO;
bool mouse_move_active = false, is_scrolling = false;
unsigned int fbo;
unsigned int colorBuffTexId;
unsigned int depthBuffTexId;
unsigned int vertexBuffTexId;
//////////////////////////////////////////////////////////////
// Funciones auxiliares
//////////////////////////////////////////////////////////////

//Declaración de CB
void renderFunc();
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mouseMotionFunc(int x, int y);

void renderCube();

//Funciones de inicialización y destrucción
void initContext(int argc, char** argv);
void initOGL();
void initObj();
void destroy();

//////////////////////////////////////////////////////////////
// Nuevas variables auxiliares
//////////////////////////////////////////////////////////////

SimpleGLShader fwShader;
SimpleGLShader* postShader;
SimpleGLShader motionShader, gaussianShader, depthShader;

typedef void (*custom_render_func)();
custom_render_func renderMode;

std::vector<custom_render_func> funcs;

bool stereo = false;
bool several_pp = false;
typedef std::vector<std::string> stringvector;
typedef stringvector::iterator stringvector_it;

//////////////////////////////////////////////////////////////
// Nuevas funciones auxiliares
//////////////////////////////////////////////////////////////
//!!Por implementar
void initShaderFw(const char *vname, const char *fname);
void initShaderPP(const char *vname, const char *fname, SimpleGLShader& postShader, stringvector attrs = stringvector(), stringvector unifs = stringvector());
void initPlane();
void initFBO();
void resizeFBO(unsigned int w, unsigned int h);
 
void motion_blur();
void gaussian_blur();
void depth_of_field();
void stereo_side_by_side();

float minDepth, maxDepth;
float motion_param = 0.5f;
int oldTimeSinceStart = 0;

std::vector<int> gaussian_list;

void changeToMotionBlur() {
	postShader = &motionShader;
	renderMode = &motion_blur;
	std::cout << "motion_blur" << std::endl;
}

void changeToGaussianBlur() {
	postShader = &gaussianShader;
	renderMode = &gaussian_blur;
	std::cout << "gaussian_blur" << std::endl;
}

void changeToDepthOfField() {
	postShader = &depthShader;
	renderMode = &depth_of_field;
	std::cout << "depth_of_field" << std::endl;
}
 
float fD;
float mD;
std::vector<std::vector<float> > convs;
int convolutionSize;
std::vector<glm::vec2> texIdx;

void configWithJSON(std::string file) {
	Jzon::Parser parser;
	Jzon::Node node = parser.parseFile(file);
	if (!node.isValid()) {
		std::cerr << parser.getError() << std::endl;
		exit(-1);
	}
	Jzon::Node arr = node.get("lists");
	std::string type;
    for (Jzon::Node::iterator it = arr.begin(); it != arr.end(); ++it) {
		type = (*it).second.toString();
		if(type == "gaussian") {
			funcs.push_back(&changeToGaussianBlur);
		} else if (type == "motion") {
			funcs.push_back(&changeToMotionBlur);
		} else if (type == "depth") {
			funcs.push_back(&changeToDepthOfField);
		}
    }
	Jzon::Node arr2 = node.get("gaussian_list");
	int type2;
    for (Jzon::Node::iterator it = arr2.begin(); it != arr2.end(); ++it) {
		type2 = (*it).second.toInt();
		gaussian_list.push_back(type2);
    }
	std::cout << gaussian_list.size() << std::endl;

	Jzon::Node depth = node.get("depth");

	fD = depth.get("focalDistFactor").toFloat();
	mD = depth.get("maxDistFactor").toFloat();

	convolutionSize = node.get("convolutionSize").toInt();

	Jzon::Node convolutions = node.get("convolution");
	for (Jzon::Node::iterator it = convolutions.begin(); it != convolutions.end(); ++it) {
		Jzon::Node arrConvolution = (*it).second;
		std::vector<float> conv;
		for(Jzon::Node::iterator it2 = arrConvolution.begin(); it2 != arrConvolution.end(); ++it2) {
			conv.push_back((*it2).second.toFloat());
		}
		convs.push_back(conv);
    }
	std::cout << convs.size();

	Jzon::Node texIdx_ = node.get("texIdx");
	bool read = false;
	for(Jzon::Node::iterator it = texIdx_.begin(); it != texIdx_.end(); ++it) {
		Jzon::Node arrIdx = (*it).second;
		glm::vec2 v2;
		read = false;
		for(Jzon::Node::iterator it2 = arrIdx.begin(); it2 != arrIdx.end(); ++it2) {
			if(!read) {
				read = true;
				v2.x = (*it2).second.toFloat();
			} else {
				v2.y = (*it2).second.toFloat();
			}
		}
		texIdx.push_back(v2);
	}
	std::cout << texIdx.size(); // TODO: Falta subirlo al shader!!
}

void configShaders() {
	initShaderFw("../shaders_P4/fwRendering.v1.vert", "../shaders_P4/fwRendering.v1.frag");

	std::vector<std::string> unif_motion;
	unif_motion.push_back("motion_param");

	std::vector<std::string> unif_gaussian;
	unif_gaussian.push_back("maskara");
	unif_gaussian.push_back("mask_size");
	for(int i = 0; i < convolutionSize; i++) {
		std::stringstream ss;
		ss << "maskara[" << i << "]";
		unif_gaussian.push_back(ss.str());
	}

	std::vector<std::string> unif_depth;
	unif_depth.push_back("vertexTex");
	unif_depth.push_back("fD");
	unif_depth.push_back("mD");
	unif_depth.push_back("near");
	unif_depth.push_back("far");
	/*unif_depth.push_back("maskara");
	unif_depth.push_back("mask_size");
	for(int i = 0; i < convolutionSize; i++) {
		std::stringstream ss;
		ss << "maskara[" << i << "]";
		unif_depth.push_back(ss.str());
	}*/

	initShaderPP("../shaders_P4/postProcessing.motion.vert", "../shaders_P4/postProcessing.motion.frag", motionShader, std::vector<std::string>(), unif_motion);
	initShaderPP("../shaders_P4/postProcessing.gaussian.vert", "../shaders_P4/postProcessing.gaussian.frag", gaussianShader, std::vector<std::string>(), unif_gaussian);
	initShaderPP("../shaders_P4/postProcessing.depth.vert", "../shaders_P4/postProcessing.depth.frag", depthShader, std::vector<std::string>(), unif_depth);	
	changeToGaussianBlur();
}

int main(int argc, char** argv) {
	configWithJSON("../config.json");
	std::locale::global(std::locale("spanish"));// acentos ;)
	
	near_ = 1.0f;
	far_ = 50.0f;

	initContext(argc, argv);
	initOGL();
	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
	// TODO: Comentar a Marcos http://stackoverflow.com/questions/22619406/why-does-glew-say-that-i-dont-have-extensions
/*	if(glewIsSupported("GL_EXT_texture_filter_anisotropic")) {
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fLargest);
		printf("App::Load > %.0fx Anistropic filtering supported.", fLargest);
	} else {
		printf("App::Load > Anistropic filtering NOT supported.");
		fLargest = -1;
	}*/

	configShaders();
	
	initPlane();
	initObj();

	initFBO();
	resizeFBO(SCREEN_SIZE);

	glutMainLoop();

	destroy();

	return 0;
}

//////////////////////////////////////////
// Funciones auxiliares 
void initContext(int argc, char** argv) {
	glutInit(&argc, argv);
	glutInitContextVersion(3, 3);
	glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowSize(SCREEN_SIZE);
	glutInitWindowPosition(0, 0);
	glutCreateWindow("Prácticas GLSL");

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		std::cout << "Error: " << glewGetErrorString(err) << std::endl;
		exit(-1);
	}

	const GLubyte *oglVersion = glGetString(GL_VERSION);
	std::cout << "This system supports OpenGL Version: " << oglVersion << std::endl;

	glutReshapeFunc(resizeFunc);
	glutDisplayFunc(renderFunc);
	glutIdleFunc(idleFunc);
	glutKeyboardFunc(keyboardFunc);
	glutMouseFunc(mouseFunc);
	glutMotionFunc(mouseMotionFunc);
}

void initOGL() {
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.2f, 0.2f, 0.2f, 0.0f);

	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glEnable(GL_CULL_FACE);

	proj = glm::perspective(glm::radians(60.0f), 1.0f, 1.0f, 50.0f);
	view = glm::mat4(1.0f);
	view[3].z = -25.0f;
}

void destroy() {
	fwShader.destroy();

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	if (fwShader.attribute("inPos") != -1) glDeleteBuffers(1, &posVBO);
	if (fwShader.attribute("inColor") != -1) glDeleteBuffers(1, &colorVBO);
	if (fwShader.attribute("inNormal") != -1) glDeleteBuffers(1, &normalVBO);
	if (fwShader.attribute("inTangent") != -1) glDeleteBuffers(1, &tgVBO);
	if (fwShader.attribute("inTexCoord") != -1) glDeleteBuffers(1, &texCoordVBO);
	glDeleteBuffers(1, &triangleIndexVBO);

	glBindVertexArray(0);
	glDeleteVertexArrays(1, &vao);

	glBindTexture(GL_TEXTURE_2D, 0);

	colorTex.deleteTexture();
	emiTex.deleteTexture();
	normalTex.deleteTexture();	
	//Destruimos los Shaders/Programa de postProceso	motionShader.destroy();	gaussianShader.destroy();	depthShader.destroy();
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &planeVertexVBO);
	glBindVertexArray(0);
	glDeleteVertexArrays(1, &planeVAO);
	//Borramos lo del FBO
	glBindFramebuffer(GL_FRAMEBUFFER,0);
	glDeleteFramebuffers(1, &fbo);
	glDeleteTextures(1, &colorBuffTexId);	glDeleteTextures(1, &vertexBuffTexId);
	glDeleteTextures(1, &depthBuffTexId);
}

void initShaderFw(const char *vname, const char *fname) {
	fwShader.load(vname, fname);

	fwShader.create();
	fwShader.link();

	fwShader.use();
		fwShader.bind_attribute("inPos", 0);
		fwShader.bind_attribute("inColor", 1);
		fwShader.bind_attribute("inNormal", 2);
		fwShader.bind_attribute("inTexCoord", 3);
		fwShader.bind_attribute("inTangent", 4);

		fwShader.add_uniform("normal");
		fwShader.add_uniform("modelView");
		fwShader.add_uniform("modelViewProj");
		
		fwShader.add_uniform("colorTex");
		fwShader.add_uniform("emiTex");
		fwShader.add_uniform("specTex");
		fwShader.add_uniform("normalTex");

		fwShader.add_attribute("inPos");
		fwShader.add_attribute("inColor");
		fwShader.add_attribute("inNormal");
		fwShader.add_attribute("inTangent");
		fwShader.add_attribute("inTexCoord");
	fwShader.unuse();
}

void initShaderPP(const char *vname, const char *fname, SimpleGLShader& shader, stringvector attrs, stringvector unifs) {
	shader.load(vname, fname);

	shader.create();
	shader.link();

	shader.use();
		shader.bind_attribute("inPos", 0);
		shader.add_uniform("colorTex");
		shader.add_attribute("inPos");
		shader.add_uniform("ts");

		for(stringvector_it it = attrs.begin(); it != attrs.end(); ++it) {
			std::cout << (*it) << std::endl;
			shader.add_attribute((*it));
		}
		for(stringvector_it it = unifs.begin(); it != unifs.end(); ++it) {
			std::cout << (*it) << std::endl;
			shader.add_uniform((*it));
		}
	shader.unuse();
 }

void initObj() {
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	if (fwShader.attribute("inPos") != -1) {
		glGenBuffers(1, &posVBO);
		glBindBuffer(GL_ARRAY_BUFFER, posVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexPos, GL_STATIC_DRAW);
		glVertexAttribPointer(fwShader.attribute("inPos"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(fwShader.attribute("inPos"));
	}

	if (fwShader.attribute("inColor") != -1) {
		glGenBuffers(1, &colorVBO);
		glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexColor, GL_STATIC_DRAW);
		glVertexAttribPointer(fwShader.attribute("inColor"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(fwShader.attribute("inColor"));
	}

	if (fwShader.attribute("inNormal") != -1) {
		glGenBuffers(1, &normalVBO);
		glBindBuffer(GL_ARRAY_BUFFER, normalVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexNormal, GL_STATIC_DRAW);
		glVertexAttribPointer(fwShader.attribute("inNormal"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(fwShader.attribute("inNormal"));
	}

	if (fwShader.attribute("inTexCoord") != -1) {
		glGenBuffers(1, &texCoordVBO);
		glBindBuffer(GL_ARRAY_BUFFER, texCoordVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 2, cubeVertexTexCoord, GL_STATIC_DRAW);
		glVertexAttribPointer(fwShader.attribute("inTexCoord"), 2, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(fwShader.attribute("inTexCoord"));
	}

	if (fwShader.attribute("inTangent") != -1) {
		glGenBuffers(1, &tgVBO);
		glBindBuffer(GL_ARRAY_BUFFER, tgVBO);
		glBufferData(GL_ARRAY_BUFFER, cubeNVertex*sizeof(float) * 3, cubeVertexTangent, GL_STATIC_DRAW);
		glVertexAttribPointer(fwShader.attribute("inTangent"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(fwShader.attribute("inTangent"));
	}

	glGenBuffers(1, &triangleIndexVBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexVBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, cubeNTriangleIndex*sizeof(unsigned int) * 3, cubeTriangleIndex, GL_STATIC_DRAW);

	model = glm::mat4(1.0f);

	colorTex.load("../img/color2.png", fLargest);
	emiTex.load("../img/emissive.png", fLargest);
	specTex.load("../img/specMap.png", fLargest);
	normalTex.load("../img/normal.png", fLargest);
}

void renderFw() {
	/**/
	fwShader.use();

	//Texturas
	if (fwShader.uniform("colorTex") != -1) {
		colorTex.select_and_bind2DTexture();
		glUniform1i(fwShader.uniform("colorTex"), 0);
	}

	if (fwShader.uniform("emiTex") != -1) {
		emiTex.select_and_bind2DTexture();
		glUniform1i(fwShader.uniform("emiTex"), 1);
	}

	if (fwShader.uniform("specTex") != -1) {
		specTex.select_and_bind2DTexture();
		glUniform1i(fwShader.uniform("specTex"), 2);
	}

	if (fwShader.uniform("normalTex") != -1) {
		normalTex.select_and_bind2DTexture();
		glUniform1i(fwShader.uniform("normalTex"), 3);
	}

	model = glm::mat4(2.0f);
	model[3].w = 1.0f;
	model = glm::rotate(model, angle, glm::vec3(1.0f, 1.0f, 0.0f));
	renderCube();

	std::srand(RAND_SEED);
	for (unsigned int i = 0; i < 10; i++) {
		float size = float(std::rand() % 3 + 1);

		glm::vec3 axis(glm::vec3(float(std::rand() % 2),
			float(std::rand() % 2), float(std::rand() % 2)));
		if (glm::all(glm::equal(axis, glm::vec3(0.0f))))
			axis = glm::vec3(1.0f);

		float trans = float(std::rand() % 7 + 3) * 1.00f + 0.5f;
		glm::vec3 transVec = axis * trans;
		transVec.x *= (std::rand() % 2) ? 1.0f : -1.0f;
		transVec.y *= (std::rand() % 2) ? 1.0f : -1.0f;
		transVec.z *= (std::rand() % 2) ? 1.0f : -1.0f;

		model = glm::rotate(glm::mat4(1.0f), angle*2.0f*size, axis);
		model = glm::translate(model, transVec);
		model = glm::rotate(model, angle*2.0f*size, axis);
		model = glm::scale(model, glm::vec3(1.0f / (size*0.7f)));
		renderCube();
	}
	//*/
}

float DIO = 0.5f;
void draw() {
	if(stereo == true) {
		glm::mat4 view_aux = view;
		glEnable(GL_BLEND);
		// Draw LEFT
		glDrawBuffer(GL_BACK_LEFT);
		glViewport(0, 0, w/2, h);
		view = glm::lookAt(glm::vec3(-DIO, 1.0f, 1.0f), glm::vec3(1.0f), glm::vec3(1.0f));
		renderFw();

		// Draw RIGHT
		glDrawBuffer(GL_BACK_RIGHT);  
		glViewport(w/2, 0, w, h);
		view = glm::lookAt(glm::vec3(+DIO, 1.0f, 1.0f), glm::vec3(0.0f), glm::vec3(0.0f));
		renderFw();
		glDisable(GL_BLEND);
		view = view_aux;
	} else {
		glViewport(0, 0, w, h);
		renderFw();
	}
}

void renderFunc() {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if(several_pp) {
		int funcs_max = funcs.size();
		for(int i = 0; i < funcs_max; i++) {
			funcs[i]();
			renderMode();
		}
	} else {
		//if(stereo) {
		//	stereo_side_by_side();
		//} else {
			renderMode();
		//}
	}

	glutSwapBuffers();
}

void renderCube() {
	glm::mat4 modelView = view * model;
	glm::mat4 modelViewProj = proj * view * model;
	glm::mat4 normal = glm::transpose(glm::inverse(modelView));

	fwShader.send_uniform_4fv("modelView", 1, GL_FALSE, &(modelView[0][0]));
	fwShader.send_uniform_4fv("modelViewProj", 1, GL_FALSE, &(modelViewProj[0][0]));
	fwShader.send_uniform_4fv("normal", 1, GL_FALSE, &(normal[0][0]));
	
	glBindVertexArray(vao);
	glDrawElements(GL_TRIANGLES, cubeNTriangleIndex * 3, GL_UNSIGNED_INT, (void*)0);
}

void resizeFunc(int width, int height) {
	glViewport(0, 0, width, height);
	proj = glm::perspective(glm::radians(60.0f), float(width) /float(height), near_, far_);

	resizeFBO(width, height);

	w = width;
	h = height;

	glutPostRedisplay();
}

void idleFunc() {
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
    int dt = currentTime - oldTimeSinceStart;
    oldTimeSinceStart = currentTime;
	angle = (angle > 3.141592f * 2.0f) ? 0 : angle + VELOCITY/3*dt;
	
	glutPostRedisplay();
}

template <typename T>
T clip(const T& n, const T& lower, const T& upper) {
	return std::max(lower, std::min(n, upper));
}

void keyboardFunc(unsigned char key, int x, int y) {
	std::cout << key << std::endl;
	switch(key) {
	case '0': idx = -1; break;
	case '1': idx = 0; break;
	case '2': idx = 1; break;
	case '3': idx = 2; break;
	case '4': idx = 3; break;
	case '5': idx = 4; break;
	case '6': idx = 5; break;
	case '7': idx = 6; break;
	case '8': idx = 7; break;
	case '9': idx = 8; break;

	// blendColor
	case 'h': 
		blend_selected = &blendColor[0];
		break;
	case 'j': 
		blend_selected = &blendColor[1];
		break;
	case 'k': 
		blend_selected = &blendColor[2];
		break;
	case 'l': 
		blend_selected = &blendColor[3];
		break;
	case 'i': 
		*blend_selected -= 0.1f;
		*blend_selected = clip<float>(*blend_selected, 0.0f, 1.0f);
		break;
	case 'o': 
		*blend_selected += 0.1f;
		*blend_selected = clip<float>(*blend_selected, 0.0f, 1.0f);
		break;
		
	case 'z': fD -= 0.5f; std::cout << fD << std::endl; break;
	case 'x': fD += 0.5f; std::cout << fD << std::endl; break;
	case 'c': mD -= 0.1f; std::cout << mD << std::endl; break;
	case 'v': mD += 0.1f; std::cout << mD << std::endl; break;

	case 'p': several_pp = !several_pp; break;

	case 'm':
		changeToMotionBlur();
		break;
	case 'g': 
		changeToGaussianBlur();
		break;
	case 'd':
		changeToDepthOfField();
		break;
	case 's': {
		stereo = !stereo;
		if(stereo) std::cout << "Activado stereo" << std::endl;
		else std::cout << "Desactivado stereo" << std::endl;
	}
	case '+': 
		motion_param += 0.1f;
		motion_param = clip<float>(motion_param, 0.0f, 1.0f);
		std::cout << motion_param << std::endl;
		break;
	case '-': 
		motion_param -= 0.1f;
		motion_param = clip<float>(motion_param, 0.0f, 1.0f);
		std::cout << motion_param << std::endl;
		break;
	}
}

void mouseFunc(int button, int state, int x, int y) {
	if (state==0) {
		mouse_move_active = true;
		if (button == 1)
			is_scrolling = true;
		oldX = x;
		oldY = y;
		std::cout << "Se ha pulsado el botón ";
	}
	else {
		mouse_move_active = false;
		if (button == 1)
			is_scrolling = false;
		std::cout << "Se ha soltado el botón ";
	}
	
	if (button == 0) std::cout << "de la izquierda del ratón " << std::endl;
	if (button == 1) std::cout << "central del ratón " << std::endl;
	if (button == 2) std::cout << "de la derecha del ratón " << std::endl;

	std::cout << "en la posición " << x << " " << y << std::endl << std::endl;
}

void mouseMotionFunc(int x, int y) {
	const float MOVEMENT_SPEED = 1.0f;
	if(mouse_move_active) {
		if(is_scrolling) {
			float moveY = y - oldY;
			moveY *= 0.1f;
			std::cout << "Nos movemos " << moveY << std::endl;
			view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f*moveY));

			oldY = y;
		} else {
			float moveX = x - oldX;
			moveX *= 0.01f;
			float moveY = y - oldY;
			moveY *= -0.01f;
			std::cout << "Nos movemos " << moveX << "-" << moveY << std::endl;
			view = glm::rotate(view, moveX, glm::vec3(1.0f, 0.0f, 0.0f));
			view = glm::rotate(view, moveY, glm::vec3(0.0f, 1.0f, 0.0f));

			oldX = x;
			oldY = y;
		}
	}
}

void initPlane(){
	glGenVertexArrays(1, &planeVAO);
	glBindVertexArray(planeVAO);

	glGenBuffers(1, &planeVertexVBO);
	glBindBuffer(GL_ARRAY_BUFFER, planeVertexVBO);
	glBufferData(GL_ARRAY_BUFFER, planeNVertex*sizeof(float) * 3, planeVertexPos, GL_STATIC_DRAW);

	glVertexAttribPointer(postShader->attribute("inPos"), 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(postShader->attribute("inPos"));
}

void initFBO() {
	glGenFramebuffers(1, &fbo);
	glGenTextures(1, &colorBuffTexId);
	glGenTextures(1, &depthBuffTexId);
	glGenTextures(1, &vertexBuffTexId);
}

void resizeFBO(unsigned int w, unsigned int h) {
	glBindTexture(GL_TEXTURE_2D, colorBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0,GL_RGBA, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, depthBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, w, h, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, vertexBuffTexId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorBuffTexId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthBuffTexId, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, vertexBuffTexId, 0);

	const GLenum buffs[2] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
	glDrawBuffers(2, buffs);

	if (GL_FRAMEBUFFER_COMPLETE != glCheckFramebufferStatus(GL_FRAMEBUFFER))
	{
		std::cerr << "Error configurando el FBO" << std::endl;
		exit(-1);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void motion_blur() {
	draw();
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); //Activamos el framebuffer por defecto
	
	fwShader.unuse();

	glEnable(GL_BLEND);
	glBlendFunc(GL_CONSTANT_COLOR, GL_CONSTANT_ALPHA);
	glBlendColor(0.5f, 0.5f, 0.5f, 0.6f); //todo: debe der configurable
	glBlendEquation(GL_FUNC_ADD);
	//Pintamos un plano
	motionShader.use();
	if (motionShader.uniform("colorTex") != -1){
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorBuffTexId);
		glUniform1i(motionShader.uniform("colorTex"), 0);
	}	if (motionShader.uniform("motion_param") != -1) {		glUniform1f(motionShader.uniform("motion_param"), motion_param);	}	if (motionShader.uniform("ts") != -1) {		glUniform2f(motionShader.uniform("ts"), 1.0/w, 1.0/h);	}
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_CONSTANT_COLOR, GL_CONSTANT_ALPHA);
	glBlendColor(blendColor[0], blendColor[1], blendColor[2], blendColor[3]);
	glBlendEquation(GL_FUNC_ADD);
	glBindVertexArray(planeVAO); //Activamos el VAO del plano
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //Pintamos el plano de manera NO INDEXADA

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	
	motionShader.unuse();
	//Fin de pintar plano
}

void apply_gaussian_blur(int idx_) {
	gaussianShader.use();	if (idx_ < convs.size() && gaussianShader.uniform("maskara") != -1) {		for(int i = 0; i < convolutionSize; i++) {
			std::stringstream ss;
			ss << "maskara[" << i << "]";			glUniform1f(gaussianShader.uniform(ss.str()), convs[idx_][i]);
		}	}

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	glBindVertexArray(planeVAO); //Activamos el VAO del plano
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //Pintamos el plano de manera NO INDEXADA

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	
	gaussianShader.unuse();
}

void gaussian_blur() {
	draw();
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); //Activamos el framebuffer por defecto
	
	fwShader.unuse();

	//Pintamos un plano
	gaussianShader.use();
	if (gaussianShader.uniform("colorTex") != -1) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorBuffTexId);
		glUniform1i(gaussianShader.uniform("colorTex"), 0);
	}	if (gaussianShader.uniform("mask_size") != -1) {		glUniform1f(gaussianShader.uniform("mask_size"), convolutionSize);	}	if (gaussianShader.uniform("ts") != -1) {		glUniform2f(gaussianShader.uniform("ts"), 1.0/w, 1.0/h);	}	if(idx == -1) {		for(int i = 0; i < gaussian_list.size(); i++) {
			apply_gaussian_blur(gaussian_list[i]);		}
	} else if (idx < convs.size()) {
		apply_gaussian_blur(idx);
	} else {
		apply_gaussian_blur(0);
	}
	//Fin de pintar plano
}

void depth_of_field() {
    glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_ALWAYS); 

	draw();
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0); //Activamos el framebuffer por defecto
    glDisable(GL_DEPTH_TEST);

	fwShader.unuse();

	//Pintamos un plano
	depthShader.use();
	
	if (depthShader.uniform("colorTex") != -1) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, colorBuffTexId);
		glUniform1i(depthShader.uniform("colorTex"), 0);
	}
	
	if (depthShader.uniform("vertexTex") != -1) {
		glActiveTexture(GL_TEXTURE0+1);
		glBindTexture(GL_TEXTURE_2D, vertexBuffTexId);
		glUniform1i(depthShader.uniform("vertexTex"), 1);
	}
	if (depthShader.uniform("mask_size") != -1) {		glUniform1f(depthShader.uniform("mask_size"), convolutionSize);	}	if (depthShader.uniform("ts") != -1) {		glUniform2f(depthShader.uniform("ts"), 1.0/w, 1.0/h);	}	int idx_ = idx;	if(idx_ < convs.size()) {		idx_ = 0;	}	if (depthShader.uniform("maskara") != -1) {		for(int i = 0; i < convolutionSize; i++) {
			std::stringstream ss;
			ss << "maskara[" << i << "]";			glUniform1f(depthShader.uniform(ss.str()), convs[idx_][i]);
		}	}

	glUniform1f(depthShader.uniform("fD"), fD);
	glUniform1f(depthShader.uniform("mD"), mD);

	glUniform1f(depthShader.uniform("near"), near_);
	glUniform1f(depthShader.uniform("far"), far_);

	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);

	glBindVertexArray(planeVAO); //Activamos el VAO del plano
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); //Pintamos el plano de manera NO INDEXADA

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	depthShader.unuse();
	//Fin de pintar plano
}

void stereo_side_by_side() {
	glEnable(GL_BLEND);
	// TODO: https://forum.processing.org/one/topic/how-to-create-side-by-side-stereoscopic-real-time-displays-in-processing.html
	view = glm::mat4(1.0f);
	view = glm::translate(view, glm::vec3(1.0f * -2, 0.0f, 0.0f));
	view[3].z = -25.0f;
	glViewport(0, 0, w/2, h);
	renderMode();
	
	view = glm::mat4(1.0f);
	view = glm::translate(view, glm::vec3(1.0f * 2, 0.0f, 0.0f));
	view[3].z = -25.0f;
	glViewport(w/2, 0, w/2, h);
	renderMode();
	glDisable(GL_BLEND);
}

﻿#include "BOX.h"
#include <IGL/IGlib.h>

#define GLM_FORCE_RADIANS
#include <gl/glut.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <gl/glut.h>
#include <iostream>

int oldTimeSinceStart = 0;
float aspect_ratio;
int oldX = 0, oldY = 0;
bool is_scrolling = false;

bool mouse_move_active = false;

//Idenficadores de los objetos de la escena
int objId =-1;

//Camera
glm::mat4 view(1.0f);
glm::mat4 proj(0.0f);

//Declaración de CB
void resizeFunc(int width, int height);
void idleFunc();
void keyboardFunc(unsigned char key, int x, int y);
void mouseFunc(int button, int state, int x, int y);
void mouseMotionFunc(int x, int y);
void animate(float dt);

bool selectLight(int select) {
	// Dos tipos de luces
	if(select == 0) {
		return IGlib::init("../shaders_P2/shader_two_lights.vert", "../shaders_P2/shader_two_lights.frag");
	} 
	// ??
	else if(select == 1) {
		return IGlib::init("../shaders_P2/shader.v6.vert", "../shaders_P2/shader.v6.frag");
	}
	// Direccional
	else if(select == 2) {
		return IGlib::init("../shaders_P2/shader_directional.vert", "../shaders_P2/shader_directional.frag");
	}
	// Focal
	else if(select == 3) {
		return IGlib::init("../shaders_P2/shader_focal.vert", "../shaders_P2/shader_focal.frag");
	}
}
//TODO: hacer especular con textura!!!
int main(int argc, char** argv) {
	std::locale::global(std::locale("spanish"));// acentos ;)

	int light = -1; 
  
	std::cout << "Select light shader: " << std::endl 
		<< "\t0) Two lights" << std::endl
		<< "\t1) Puntual" << std::endl
		<< "\t2) Directional" << std::endl
		<< "\t3) Focal" << std::endl; 
	
	while(light < 0 || light > 4) {
		std::cout << "=> ";
		std::cin >> light; // input the light 
	}

	if (!selectLight(light)) {
		return -1;
	}
   
	//CBs
	IGlib::setResizeCB(resizeFunc);
	IGlib::setIdleCB(idleFunc);
	IGlib::setKeyboardCB(keyboardFunc);
	IGlib::setMouseCB(mouseFunc);

	//Creamos el objeto que vamos a visualizar
	objId = IGlib::createObj(cubeNTriangleIndex, cubeNVertex, cubeTriangleIndex, 
			cubeVertexPos, cubeVertexColor, cubeVertexNormal,cubeVertexTexCoord, cubeVertexTangent);
	IGlib::addColorTex(objId, "../img/color2.png");
	IGlib::addEmissiveTex(objId, "../img/emissive.png");
	IGlib::addSpecularTex(objId, "../img/specMap.png");
	IGlib::addNormalTex(objId, "../img/normal.png");
		
	glm::mat4 modelMat = glm::mat4(1.0f);
	IGlib::setModelMat(objId, modelMat);
	
	//CBs
	IGlib::setIdleCB(idleFunc);
	IGlib::setResizeCB(resizeFunc);
	IGlib::setKeyboardCB(keyboardFunc);
	IGlib::setMouseCB(mouseFunc);
  	IGlib::setMouseMoveCB(mouseMotionFunc);
	
	//Mainloop
	IGlib::mainLoop();
	IGlib::destroy();
	return 0;
}

void resizeFunc(int width, int height) {
	//Ajusta el aspect ratio al tamańo de la venta
	//Tener en cuenta la división por 0...
	//Se ajusta la cámara
	//Si no se da valor se cogen valores por defecto
	aspect_ratio = (float)height/(float)width;
	std::cout << aspect_ratio << std::endl;
	view[3].z = -10.0f;
	float n = 0.5f;
	float f =  50.0f;

	/*proj[0].x =  1.0f / (tan (3.14159f / 6.0f) * aspect_ratio);
	proj[1].y = proj[0].x / aspect_ratio;
	proj[2].z = (-n - f)/(f-n);
	proj[2].w = -1.0f;
	proj[3].z = (-2.0f * n * f) / (f-n);*/

	proj = glm::perspective(glm::radians(60.0f), float(width) /float(height), n, f);

	IGlib::setProjMat(proj);
	IGlib::setViewMat(view);
}

void animate(float dt) {
	glm::mat4 modelMat(1.0f);
	static float angle = 0.0f;
	angle = (angle > 3.141592f * 2.0f) ? 0 : angle + 0.001f*dt;
	
	modelMat = glm::rotate(modelMat, angle, glm::vec3(1.0f, 1.0f, 0.0f));

	IGlib::setModelMat(objId, modelMat);
}

void idleFunc() {
	int currentTime = glutGet(GLUT_ELAPSED_TIME);
    int deltaTime = currentTime - oldTimeSinceStart;
    oldTimeSinceStart = currentTime;

	animate(deltaTime);
}

void keyboardFunc(unsigned char key, int x, int y) {
	const float MOVEMENT_SPEED	=	1.0f;
	const float ZOOM_SPEED		=	0.1f;
	const float ANGLE_ROTATION	=	0.01f;
	std::cout << "Se ha pulsado la tecla " << key << std::endl << std::endl;

	switch(key){
		//Right movement
	case 'd':
		view = glm::translate(view, glm::vec3(1.0 * -MOVEMENT_SPEED, 0.0f, 0.0f));
		break;
		//Left movement
	case 'a':
		view = glm::translate(view, glm::vec3(1.0 * MOVEMENT_SPEED, 0.0f, 0.0f));
		break;
		//Up movement
	case 'w':
		view = glm::translate(view, glm::vec3(0.0f, 1.0f * -MOVEMENT_SPEED, 0.0f));
		break;
		//Bottom movement
	case 's': 
		view = glm::translate(view, glm::vec3(0.0f, 1.0f * MOVEMENT_SPEED, 0.0f));
		break;
		//Zoom in
	case 'z':
		view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f * ZOOM_SPEED));
		break;
		//Zoom out
	case 'x':
		view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f * -ZOOM_SPEED));
		break;
		//Rotate left
	case 'g':
		view = glm::rotate(view, ANGLE_ROTATION, glm::vec3(0.0f, 1.0f, 0.0f));
		break;
		//Rotate right
	case 'h':
		view = glm::rotate(view, -ANGLE_ROTATION, glm::vec3(0.0f, 1.0f, 0.0f));
		break;
	case 'v':
		view = glm::rotate(view, ANGLE_ROTATION, glm::vec3(0.0f, 0.0f, 1.0f));
		break;
	case 'b':
		view = glm::rotate(view, -ANGLE_ROTATION, glm::vec3(0.0f, 0.0f, 1.0f));
		break;
	}

	IGlib::setViewMat(view);

}

void mouseFunc(int button, int state, int x, int y) {
	if (state==0) {
		mouse_move_active = true;
		if (button == 1)
			is_scrolling = true;
		oldX = x;
		oldY = y;
		std::cout << "Se ha pulsado el botón ";
	}
	else {
		mouse_move_active = false;
		if (button == 1)
			is_scrolling = false;
		std::cout << "Se ha soltado el botón ";
	}
	
	if (button == 0) std::cout << "de la izquierda del ratón " << std::endl;
	if (button == 1) std::cout << "central del ratón " << std::endl;
	if (button == 2) std::cout << "de la derecha del ratón " << std::endl;

	std::cout << "en la posición " << x << " " << y << std::endl << std::endl;
}

void mouseMotionFunc(int x, int y) {
	const float MOVEMENT_SPEED = 1.0f;
	if(mouse_move_active) {
		if(is_scrolling) {
			float moveY = y - oldY;
			moveY *= 0.1f;
			std::cout << "Nos movemos " << moveY << std::endl;
			view = glm::translate(view, glm::vec3(0.0f, 0.0f, 1.0f*moveY));
			IGlib::setViewMat(view);
			oldY = y;
		} else {
			float moveX = x - oldX;
			moveX *= 0.01f;
			float moveY = y - oldY;
			moveY *= -0.01f;
			std::cout << "Nos movemos " << moveX << "-" << moveY << std::endl;
			view = glm::rotate(view, moveX, glm::vec3(1.0f, 0.0f, 0.0f));
			view = glm::rotate(view, moveY, glm::vec3(0.0f, 1.0f, 0.0f));
			IGlib::setViewMat(view);
			oldX = x;
			oldY = y;
		}
	}
}
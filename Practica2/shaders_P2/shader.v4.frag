#version 330 core

out vec4 outColor;

in vec3 color;
in vec3 outPos;
in vec3 outNormal;
in vec2 outTexCoord;

uniform mat4 normal;
uniform mat4 modelView;
uniform sampler2D colorTex;
uniform sampler2D emiTex;
uniform sampler2D specularTex;

//Luz
//Intensidad ambiental
vec3 Ia = vec3(0.3);
//Intensidad de la luz
vec3 Il = vec3(0.8);
//Posicion de la luz
vec3 Pl = vec3(0.0); //Suponemos que est� en coordenadas de la c�mara

//Material
vec3 Ka; //Coeficiente de luz ambiental
vec3 Kd; //Coeficiente de luz difusa
vec3 Ks; //Coeficiente de luz especular
vec3 Ke; //Emissive

float n = 30.0;
vec3 Po; //Posici�n del objeto
vec3 N;

vec3 shade();

void main(){
	
	Ka = texture(colorTex, outTexCoord).rgb; //Seleccionamos ese color para no inventarnos ninguno y utilizar el que nos ha dado el "usuario"
	Kd = Ka;
	Ks = texture(specularTex, outTexCoord).rgb;
	Ke = texture(emiTex, outTexCoord).rgb;
	Po =  vec3( vec4(outPos,1)); //inPos est� en coordenadas del objeto
	N = normalize(outNormal);

	outColor = vec4(shade(), 1.0);
}

vec3 shade(){
	vec3 colorAmbiental = Ia*Ka;
	vec3 colorDifuso;
	vec3 L = normalize(Pl-Po);
	//Intensidad luminica, coeficiente de iluminacion difusa...y producto escalar N y L.
	colorDifuso = clamp(Il * Kd * dot(N,L), 0, 1);

	vec3 colorEspecular;
	vec3 R = reflect(-L, N); //R es el rayo reflejado. Si L y N est�n normalizados no hace falta normalizarlo
	vec3 V = normalize(-Po); //V es el rayo que va desde el punto hasta la c�mara
	float factorEspecular = max(dot(R,V),0.0001); 
	factorEspecular = pow(factorEspecular, n); //Ojo! si x <= 0 el resultado es indefinido!!
	colorEspecular = Il * Ks * factorEspecular;
	//return N;
   return colorAmbiental + colorDifuso + colorEspecular + Ke;

}
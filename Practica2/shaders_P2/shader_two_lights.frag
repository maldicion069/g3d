#version 330 core

//Shader con dos fuentes de luz

out vec4 outColor;

in vec3 color;
in vec3 outPos;
in vec3 outNormal;
in vec2 outTexCoord;

uniform mat4 normal;
uniform mat4 modelView;
uniform sampler2D colorTex;
uniform sampler2D emiTex;
uniform sampler2D specularTex;

//Luz
//Intensidad ambiental
vec3 Ia = vec3(0.3);
//Intensidad de la luz
vec3 Il1 = vec3(0.8); //Para la luz 1
vec3 Il2 = vec3(0.8); //Para la luz 2
//Posicion de la luz
vec3 Pl = vec3(20.0, 0.0, -10.0); //Suponemos que est� en coordenadas de la c�mara
vec3 Pl2 = vec3(-20.0, 0.0, 0.0); //Suponemos que est� en coordenadas de la c�mara

//Material
vec3 Ka; //Coeficiente de luz ambiental
vec3 Kd; //Coeficiente de luz difusa
vec3 Ks; //Coeficiente de luz especular
vec3 Ke; //Emissive

float n = 30.0;
vec3 Po; //Posici�n del objeto
vec3 N;

vec3 shade();

void main(){
	
	Ka = texture(colorTex, outTexCoord).rgb; //Seleccionamos ese color para no inventarnos ninguno y utilizar el que nos ha dado el "usuario"
	Kd = Ka;
	Ks = texture(specularTex, outTexCoord).rgb;
	Ke = texture(emiTex, outTexCoord).rgb;
	Po =  vec3( vec4(outPos,1)); //inPos est� en coordenadas del objeto
	N = normalize(outNormal);

	outColor = vec4(shade(), 1.0);
}

vec3 shade(){
	vec3 colorAmbiental = Ia*Ka;
	vec3 colorDifusoL1;
	vec3 colorDifusoL2;
	vec3 L1 = normalize(Pl-Po);
	vec3 L2 = normalize(Pl2-Po);
	//Intensidad luminica, coeficiente de iluminacion difusa...y producto escalar N y L.
	colorDifusoL1 = clamp(Il1 * Kd * dot(N,L1), 0, 1) * vec3(0.0,0.0, 1.0); //Multiplicamos por un vec3 para darle un color a la luz
	colorDifusoL2 = clamp(Il2 * Kd * dot(N,L2), 0, 1) * vec3(1.0,0.0,0.0);
	vec3 colorDifusoTotal = Kd * (colorDifusoL1 + colorDifusoL2);

	vec3 colorEspecularTotal;
	vec3 colorEspecular1;
	vec3 colorEspecular2;

	vec3 R1 = reflect(-L1, N); //R es el rayo reflejado. Si L y N est�n normalizados no hace falta normalizarlo
	vec3 R2 = reflect(-L2, N); //R es el rayo reflejado. Si L y N est�n normalizados no hace falta normalizarlo
	vec3 V = normalize(-Po); //V es el rayo que va desde el punto hasta la c�mara

	float factorEspecular1 = max(dot(R1,V), 0.0001); 
	float factorEspecular2 = max(dot(R2,V), 0.0001); 

	factorEspecular1 = pow(factorEspecular1, n); //Ojo! si x <= 0 el resultado es indefinido!!
	factorEspecular2 = pow(factorEspecular2, n); //Ojo! si x <= 0 el resultado es indefinido!!

	colorEspecular1 = Il1 * factorEspecular1;
	colorEspecular2 = Il2 * factorEspecular2;
	colorEspecularTotal = Ks * colorEspecular1 * colorEspecular2;
	
    return colorAmbiental + colorDifusoTotal + colorEspecularTotal + Ke;
}
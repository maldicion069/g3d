#version 330 core

in vec3 inPos;	
in vec3 inColor;
in vec3 inNormal;

uniform mat4 normal;
uniform mat4 modelView;
uniform mat4 modelViewProj;

out vec3 color;

//Luz
//Intensidad ambiental
vec3 Ia = vec3(0.3);
//Intensidad de la luz
vec3 Il = vec3(0.8);
//Posicion de la luz
vec3 Pl = vec3(0.0); //Suponemos que est� en coordenadas de la c�mara

//Material
//Coeficiente de luz ambiental
vec3 Ka;
//Coeficiente de luz difusa
vec3 Kd;
vec3 Ks; //Coeficiente de luz especular
float n = 30.0;
vec3 Po; //Posici�n del objeto
vec3 N;

vec3 shade();

void main(){

	Ka = inColor; //Seleccionamos ese color para no inventarnos ninguno y utilizar el que nos ha dado el "usuario"
	Kd = inColor;
	Ks = vec3(1.0,1.0,1.0);
	Po =  vec3(modelView * vec4(inPos,1)); //inPos est� en coordenadas del objeto
	N = normalize( vec3(normal * vec4(inNormal,0)) );
	color = shade();
	gl_Position =  modelViewProj * vec4 (inPos,1.0);
}

vec3 shade(){
	vec3 colorAmbiental = Ia*Ka;
	vec3 colorDifuso;
	vec3 L = normalize(Pl-Po);
	//Intensidad luminica, coeficiente de iluminacion difusa...y producto escalar N y L.
	colorDifuso = clamp(Il * Kd * dot(N,L), 0, 1);

	vec3 colorEspecular;
	vec3 R = reflect(-L, N); //R es el rayo reflejado. Si L y N est�n normalizados no hace falta normalizarlo
	vec3 V = normalize(-Po); //V es el rayo que va desde el punto hasta la c�mara
	float factorEspecular = max(dot(R,V),0.0001); 
	factorEspecular = pow(factorEspecular, n); //Ojo! si x <= 0 el resultado es indefinido!!
	colorEspecular = Il * Ks * factorEspecular;


	return colorAmbiental + colorDifuso + colorEspecular;
}
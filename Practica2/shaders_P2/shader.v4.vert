#version 330 core

in vec3 inPos;	
in vec3 inColor;
in vec3 inNormal;
in vec2 inTexCoord;	

uniform mat4 normal;
uniform mat4 modelView;
uniform mat4 modelViewProj;

out vec3 color;
out vec3 outNormal;
out vec3 outPos;
out vec2 outTexCoord;

void main(){

	//Po =  vec3(modelView * vec4(inPos,1)); //inPos est� en coordenadas del objeto
	//N = normalize( vec3(normal * vec4(inNormal,0)) );

	outNormal = normalize( vec3(normal * vec4(inNormal,0)) );
	outPos = vec3(modelView * vec4(inPos,1));
	outTexCoord = inTexCoord;

	color = inColor;
	

	gl_Position =  modelViewProj * vec4 (inPos,1.0);
}
